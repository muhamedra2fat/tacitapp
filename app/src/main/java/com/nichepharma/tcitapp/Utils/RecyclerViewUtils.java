package com.nichepharma.tcitapp.Utils;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class RecyclerViewUtils {
    public static LinearLayoutManager setupRecyclerView(Context context, RecyclerView.Adapter mAdapter, RecyclerView recyclerView, boolean isVertical) {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        if (!isVertical)
            mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        return mLayoutManager;
    }

    public static GridLayoutManager setupRecyclerView(Context context, RecyclerView.Adapter mAdapter, RecyclerView recyclerView, int numOfColumns) {
        GridLayoutManager mLayoutManager = new GridLayoutManager(context, numOfColumns);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        return mLayoutManager;
    }
}
