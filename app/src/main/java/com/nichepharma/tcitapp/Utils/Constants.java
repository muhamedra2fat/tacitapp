package com.nichepharma.tcitapp.Utils;

public class Constants {

    public static final String REFERENCE_PATH = "ref_path";

    public static String COMPANY_ID = "";
    public static String USER_ID = "";
    public static String TOKEN = "";
    public final static String APP_URL = "http://www.tacitapp.com/Yahia/";
    public final static String PRODUCT_URL = "http://tacitapp.herokuapp.com/uploads/products/";
    //    Extras
    public final static String SLIDES_MAP = "slides_map";
    public final static String PRODUCT_NAME = "product_name";
    public final static String PRODUCT_OBJ = "product_obj";
    public final static String CUSTOMERS = "customers";
    public final static String SYNCHRONIZATION_OBJ = "sync_obj";
    public final static String IS_HOSPITAL = "is_hospital";
    public final static String PRODUCT_ID = "product_id";
    public static final String SYNC_POSITION = "sync_position";
    //    keys
    public final static String PRIVATE_MARKET = "private_market";
    public final static String HOSPITALS = "hospitals";
    public final static String PHARMACY = "pharmacy";
    //    feedback types
    public final static int ADD_FEEDBACK = 1;
    public final static int EDIT_FEEDBACK = 2;

}

