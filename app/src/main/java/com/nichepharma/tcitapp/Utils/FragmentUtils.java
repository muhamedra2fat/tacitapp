
package com.nichepharma.tcitapp.Utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


import com.nichepharma.tcitapp.R;

public class FragmentUtils {


    public static void startFragment(Context context, Fragment fragment, int resLayout, boolean withBackStack) {

        if (fragment != null) {
            FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

            final FragmentTransaction transaction = fragmentManager.beginTransaction()
                    .replace(resLayout, fragment, fragment.getClass().getName());
            if (withBackStack)
                transaction.addToBackStack(null);
            transaction.setCustomAnimations(R.anim.enter_fragment_animation,R.anim.exit_fragment_animation);
            transaction.commitAllowingStateLoss();
        }


    }
}
