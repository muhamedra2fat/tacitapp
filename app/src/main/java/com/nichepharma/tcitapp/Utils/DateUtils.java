package com.nichepharma.tcitapp.Utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    public static String getSessionTimeStamp() {
        Date date = new Date();
        String timeStamp = new SimpleDateFormat(" MMM d, yyyy HH:mm a", Locale.ENGLISH).format(date);
        return timeStamp;

    }
    public static String getProductTimeStamp() {
        Date date = new Date();
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS Z", Locale.ENGLISH).format(date);
        return timeStamp;

    }

}

