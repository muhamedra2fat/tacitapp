package com.nichepharma.tcitapp.Repository.SharedPreference;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public class MySharedPreferenceHelper {
    private SharedPreferences prefs;

    public MySharedPreferenceHelper(@NonNull SharedPreferences prefs) {
        this.prefs = prefs;

    }

    public void saveToSharedPreference(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void saveToSharedPreference(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public void saveToSharedPreference(String key, boolean value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public String getFromSharedPreference(String key, String defaultValue) {
        return prefs.getString(key, defaultValue);
    }

    public boolean getFromSharedPreference(String key, boolean defaultValue) {
        return prefs.getBoolean(key, defaultValue);
    }

    public void clearSharedPreference() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }


}
