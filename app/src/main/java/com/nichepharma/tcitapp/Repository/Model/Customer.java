package com.nichepharma.tcitapp.Repository.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Customer implements Parcelable {

    @SerializedName("speciality")
    private String speciality;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("phone")
    private String phone;

    @SerializedName("doctors")
    private List<Customer> doctors;

    @SerializedName("center")
    private String center;

    @SerializedName("__v")
    private int V;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private Location location;

    @SerializedName("departments")
    private List<Object> departments;

    @SerializedName("_id")
    private String id;

    @SerializedName("email")
    private String email;

    @SerializedName("updatedAt")
    private String updatedAt;

    @SerializedName("type")
    private String type;

    @SerializedName("class")
    private String classs;


    protected Customer(Parcel in) {
        speciality = in.readString();
        createdAt = in.readString();
        phone = in.readString();
        doctors = in.createTypedArrayList(Customer.CREATOR);
        center = in.readString();
        V = in.readInt();
        name = in.readString();
        id = in.readString();
        email = in.readString();
        updatedAt = in.readString();
        type = in.readString();
        classs = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(speciality);
        dest.writeString(createdAt);
        dest.writeString(phone);
        dest.writeTypedList(doctors);
        dest.writeString(center);
        dest.writeInt(V);
        dest.writeString(name);
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(updatedAt);
        dest.writeString(type);
        dest.writeString(classs);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setDoctors(List<Customer> doctors) {
        this.doctors = doctors;
    }

    public List<Customer> getDoctors() {
        return doctors;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getCenter() {
        return center;
    }

    public void setV(int V) {
        this.V = V;
    }

    public int getV() {
        return V;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setDepartments(List<Object> departments) {
        this.departments = departments;
    }

    public List<Object> getDepartments() {
        return departments;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getClasss() {
        return classs;
    }

    public void setClasss(String classs) {
        this.classs = classs;
    }

    @Override
    public String toString() {
        return
                "ResponseItem{" +
                        "speciality = '" + speciality + '\'' +
                        ",createdAt = '" + createdAt + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",doctors = '" + doctors + '\'' +
                        ",center = '" + center + '\'' +
                        ",__v = '" + V + '\'' +
                        ",name = '" + name + '\'' +
                        ",location = '" + location + '\'' +
                        ",departments = '" + departments + '\'' +
                        ",_id = '" + id + '\'' +
                        ",email = '" + email + '\'' +
                        ",updatedAt = '" + updatedAt + '\'' +
                        "}";
    }

}