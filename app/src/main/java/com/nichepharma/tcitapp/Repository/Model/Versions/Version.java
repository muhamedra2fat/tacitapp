package com.nichepharma.tcitapp.Repository.Model.Versions;

import com.nichepharma.tcitapp.Repository.Model.Product;

public class Version {

    private String name;
    private String id;
    private double version;
    private int isLoading ;

    public Version(String name, String id, double version) {
        this.name = name;
        this.id = id;
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getVersion() {
        return version;
    }

    public void setVersion(double version) {
        this.version = version;
    }

    public int isLoading() {
        return isLoading;
    }

    public void setLoading(int loading) {
        isLoading = loading;
    }
}
