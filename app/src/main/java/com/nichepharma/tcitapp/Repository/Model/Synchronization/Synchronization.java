package com.nichepharma.tcitapp.Repository.Model.Synchronization;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.nichepharma.tcitapp.Repository.Model.Location;
import com.nichepharma.tcitapp.Utils.DateUtils;


public class Synchronization implements Serializable{

    @SerializedName("duration")
    private int duration;

    @SerializedName("company_id")
    private String companyId;

    @SerializedName("calls")
    private List<CallsItem> calls;

    @SerializedName("location")
    private Location location;

    @SerializedName("visit_type")
    private String visitType;

    @SerializedName("_id")
    private String id;

    @SerializedName("user")
    private User user;

    @SerializedName("device")
    private String device;

    @SerializedName("customer")
    private Customer customer;

    public Synchronization() {
        id = DateUtils.getSessionTimeStamp() ;
        device = "Android";
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCalls(List<CallsItem> calls) {
        this.calls = calls;
    }

    public List<CallsItem> getCalls() {
        if (calls == null)
            calls = new ArrayList<>();
        return calls;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDevice() {
        return device;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        return
                "Synchronization{" +
                        "duration = '" + duration + '\'' +
                        ",company_id = '" + companyId + '\'' +
                        ",calls = '" + calls + '\'' +
                        ",location = '" + location + '\'' +
                        ",visit_type = '" + visitType + '\'' +
                        ",_id = '" + id + '\'' +
                        ",user = '" + user + '\'' +
                        ",device = '" + device + '\'' +
                        ",customer = '" + customer + '\'' +
                        "}";
    }
}