package com.nichepharma.tcitapp.Repository.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HaveActionItem implements Serializable{

	@SerializedName("positionY")
	private Object positionY;

	@SerializedName("delay")
	private double delay;

	@SerializedName("name")
	private String name;

	@SerializedName("positionX")
	private Object positionX;

	@SerializedName("animationType")
	private String animationType;

	public void setPositionY(Object positionY){
		this.positionY = positionY;
	}

	public Object getPositionY(){
		return positionY;
	}

	public void setDelay(double delay){
		this.delay = delay;
	}

	public double getDelay(){
		return delay;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setPositionX(Object positionX){
		this.positionX = positionX;
	}

	public Object getPositionX(){
		return positionX;
	}

	public void setAnimationType(String animationType){
		this.animationType = animationType;
	}

	public String getAnimationType(){
		return animationType;
	}

	@Override
 	public String toString(){
		return 
			"HaveActionItem{" + 
			"positionY = '" + positionY + '\'' + 
			",delay = '" + delay + '\'' + 
			",name = '" + name + '\'' + 
			",positionX = '" + positionX + '\'' + 
			",animationType = '" + animationType + '\'' + 
			"}";
		}
}