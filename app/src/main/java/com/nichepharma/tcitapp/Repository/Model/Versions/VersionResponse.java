package com.nichepharma.tcitapp.Repository.Model.Versions;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VersionResponse {
    @SerializedName("response")
    private List<Version> response;

    @SerializedName("status")
    private String status;

    public VersionResponse() {
    }

    public List<Version> getResponse() {
        return response;
    }

    public void setResponse(List<Version> response) {
        this.response = response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
