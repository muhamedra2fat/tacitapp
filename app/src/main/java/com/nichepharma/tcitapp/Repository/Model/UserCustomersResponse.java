package com.nichepharma.tcitapp.Repository.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UserCustomersResponse {

	@SerializedName("response")
	private List<Customer> response;

	@SerializedName("status")
	private String status;

	public void setResponse(List<Customer> response){
		this.response = response;
	}

	public List<Customer> getResponse(){
		return response;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"UserCustomerResponse{" + 
			"response = '" + response + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}