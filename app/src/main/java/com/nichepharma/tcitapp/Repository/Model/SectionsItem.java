package com.nichepharma.tcitapp.Repository.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SectionsItem{

	@SerializedName("slides")
	private List<SlidesItem> slides;

	@SerializedName("name")
	private String name;

	@SerializedName("_id")
	private String id;

	public void setSlides(List<SlidesItem> slides){
		this.slides = slides;
	}

	public List<SlidesItem> getSlides(){
		return slides;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SectionsItem{" + 
			"slides = '" + slides + '\'' + 
			",name = '" + name + '\'' + 
			",_id = '" + id + '\'' + 
			"}";
		}
}