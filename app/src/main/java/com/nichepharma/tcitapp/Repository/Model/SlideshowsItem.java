package com.nichepharma.tcitapp.Repository.Model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SlideshowsItem implements Serializable {

	@SerializedName("image")
	private List<ImageItem> image;

	@SerializedName("isEditable")
	private String isEditable;

	@SerializedName("Slideposition")
	private String slideposition;

	@SerializedName("name")
	private String name;

	@SerializedName("slideBKG")
	private String slideBKG;

	@SerializedName("id")
	private String id;

	public void setImage(List<ImageItem> image){
		this.image = image;
	}

	public List<ImageItem> getImage(){
		return image;
	}

	public void setIsEditable(String isEditable){
		this.isEditable = isEditable;
	}

	public String getIsEditable(){
		return isEditable;
	}

	public void setSlideposition(String slideposition){
		this.slideposition = slideposition;
	}

	public String getSlideposition(){
		return slideposition;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setSlideBKG(String slideBKG){
		this.slideBKG = slideBKG;
	}

	public String getSlideBKG(){
		return slideBKG;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SlideshowsItem{" + 
			"image = '" + image + '\'' + 
			",isEditable = '" + isEditable + '\'' + 
			",slideposition = '" + slideposition + '\'' + 
			",name = '" + name + '\'' + 
			",slideBKG = '" + slideBKG + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}