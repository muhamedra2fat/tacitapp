package com.nichepharma.tcitapp.Repository.Model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Product implements Serializable {

    public final static int DOWNLOADED_STATUS = 1;
    public final static int NOT_DOWNLOADED_STATUS = 0;
    public final static int ERROR_STATUS = 3;
    public final static int LOADING_STATUS = 4;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("company_id")
    private String companyId;

    @SerializedName("versions")
    private List<Object> versions;

    @SerializedName("__v")
    private double V;

    @SerializedName("name")
    private String name;

    @SerializedName("_id")
    private String id;

    @SerializedName("updatedAt")
    private String updatedAt;

    private String path_name;
    @SerializedName("icon_path")
    private String icon_path;

    public String getPath_name() {
        return path_name;
    }

    public void setPath_name(String path_name) {
        this.path_name = path_name;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setVersions(List<Object> versions) {
        this.versions = versions;
    }

    public List<Object> getVersions() {
        return versions;
    }

    public void setV(double V) {
        this.V = V;
    }

    public double getV() {
        return V;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getIcon_path() {
        return icon_path;
    }

    public void setIcon_path(String icon_path) {
        this.icon_path = icon_path;
    }

    @Override
    public String toString() {
        return
                "ResponseItem{" +
                        "createdAt = '" + createdAt + '\'' +
                        ",quantity = '" + quantity + '\'' +
                        ",company_id = '" + companyId + '\'' +
                        ",versions = '" + versions + '\'' +
                        ",__v = '" + V + '\'' +
                        ",name = '" + name + '\'' +
                        ",_id = '" + id + '\'' +
                        ",updatedAt = '" + updatedAt + '\'' +
                        "}";
    }
}