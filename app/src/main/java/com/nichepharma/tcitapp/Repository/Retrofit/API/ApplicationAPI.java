package com.nichepharma.tcitapp.Repository.Retrofit.API;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.nichepharma.tcitapp.Repository.Model.LoginResponse;
import com.nichepharma.tcitapp.Repository.Model.ProductsResponse;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.SynchronizationResponse;
import com.nichepharma.tcitapp.Repository.Model.UserCustomersResponse;
import com.nichepharma.tcitapp.Repository.Model.Versions.VersionResponse;
import com.nichepharma.tcitapp.Repository.Retrofit.ApiFactory;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplicationAPI {
    private IApplicationAPI iApplicationAPI;


    public ApplicationAPI() {
        iApplicationAPI = ApiFactory.getClient().create(IApplicationAPI.class);
    }

    public LiveData<LoginResponse> login(String username, String password) {
        final MutableLiveData<LoginResponse> loginResponseMutableLiveData = new MutableLiveData<>();

        iApplicationAPI.login(username, password).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body() != null) {
                    loginResponseMutableLiveData.setValue(response.body());

                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginResponseMutableLiveData.setValue(null);
            }
        });
        return loginResponseMutableLiveData;


    }

    public LiveData<SynchronizationResponse> sendVisitsAndCalls(Synchronization synchronization) {
        final MutableLiveData<SynchronizationResponse> synchronizationResponseMutableLiveData = new MutableLiveData<>();

        iApplicationAPI.sendVisitsAndCalls(synchronization).enqueue(new Callback<SynchronizationResponse>() {
            @Override
            public void onResponse(Call<SynchronizationResponse> call, Response<SynchronizationResponse> response) {
                if (response.body() != null) {
                    synchronizationResponseMutableLiveData.setValue(response.body());

                } else
                    synchronizationResponseMutableLiveData.setValue(null);
            }

            @Override
            public void onFailure(Call<SynchronizationResponse> call, Throwable t) {
                synchronizationResponseMutableLiveData.setValue(null);
            }
        });
        return synchronizationResponseMutableLiveData;


    }

    public LiveData<ProductsResponse> getProducts(String companyId) {
        final MutableLiveData<ProductsResponse> productsResponseMutableLiveData = new MutableLiveData<>();

        iApplicationAPI.getProducts(companyId).enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                if (response.body() != null) {
                    productsResponseMutableLiveData.setValue(response.body());

                }

            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                productsResponseMutableLiveData.setValue(null);
            }
        });
        return productsResponseMutableLiveData;


    }

    public LiveData<UserCustomersResponse> getUserCustomers(String companyId, String userId) {
        final MutableLiveData<UserCustomersResponse> userCustomersResponseMutableLiveData = new MutableLiveData<>();

        iApplicationAPI.getUserCustomers(companyId, userId).enqueue(new Callback<UserCustomersResponse>() {
            @Override
            public void onResponse(Call<UserCustomersResponse> call, Response<UserCustomersResponse> response) {
                    userCustomersResponseMutableLiveData.setValue(response.body());



            }

            @Override
            public void onFailure(Call<UserCustomersResponse> call, Throwable t) {
                userCustomersResponseMutableLiveData.setValue(null);
            }
        });
        return userCustomersResponseMutableLiveData;
    }

    public LiveData<VersionResponse> getVersions(String companyId) {
        final MutableLiveData<VersionResponse> userCustomersResponseMutableLiveData = new MutableLiveData<>();

        iApplicationAPI.getVersions(companyId).enqueue(new Callback<VersionResponse>() {
            @Override
            public void onResponse(Call<VersionResponse> call, Response<VersionResponse> response) {
                userCustomersResponseMutableLiveData.setValue(response.body());


            }

            @Override
            public void onFailure(Call<VersionResponse> call, Throwable t) {
                userCustomersResponseMutableLiveData.setValue(null);
            }
        });
        return userCustomersResponseMutableLiveData;
    }

    public MutableLiveData<ResponseBody> downloadFile(String url) {
        final MutableLiveData<ResponseBody> userCustomersResponseMutableLiveData = new MutableLiveData<>();
        iApplicationAPI.downloadFile(url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d("download file", "server contacted and has file");

                    userCustomersResponseMutableLiveData.setValue(response.body());
                } else {
                    userCustomersResponseMutableLiveData.setValue(null);
                    Log.d("download file", "server contact failed");

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                userCustomersResponseMutableLiveData.setValue(null);
            }
        });
        return userCustomersResponseMutableLiveData;
    }

}
