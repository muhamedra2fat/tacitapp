package com.nichepharma.tcitapp.Repository.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductModel{

	@SerializedName("analytics")
	private String analytics;

	@SerializedName("slideshows")
	private List<SlideshowsItem> slideshows;

	@SerializedName("companyLogo")
	private List<CompanyLogoItem> companyLogo;

	@SerializedName("appID")
	private String appID;

	@SerializedName("name")
	private String name;

	public void setAnalytics(String analytics){
		this.analytics = analytics;
	}

	public String getAnalytics(){
		return analytics;
	}

	public void setSlideshows(List<SlideshowsItem> slideshows){
		this.slideshows = slideshows;
	}

	public List<SlideshowsItem> getSlideshows(){
		return slideshows;
	}

	public void setCompanyLogo(List<CompanyLogoItem> companyLogo){
		this.companyLogo = companyLogo;
	}

	public List<CompanyLogoItem> getCompanyLogo(){
		return companyLogo;
	}

	public void setAppID(String appID){
		this.appID = appID;
	}

	public String getAppID(){
		return appID;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"ProductModel{" + 
			"analytics = '" + analytics + '\'' + 
			",slideshows = '" + slideshows + '\'' + 
			",companyLogo = '" + companyLogo + '\'' + 
			",appID = '" + appID + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}