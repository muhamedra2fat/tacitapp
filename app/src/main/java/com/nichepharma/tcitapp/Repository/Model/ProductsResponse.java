package com.nichepharma.tcitapp.Repository.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProductsResponse{

	@SerializedName("response")
	private List<Product> response;

	@SerializedName("status")
	private String status;

	public void setResponse(List<Product> response){
		this.response = response;
	}

	public List<Product> getResponse(){
		return response;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProductsResponse{" + 
			"response = '" + response + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}