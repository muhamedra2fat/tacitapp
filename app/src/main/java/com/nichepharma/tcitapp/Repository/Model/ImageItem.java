package com.nichepharma.tcitapp.Repository.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ImageItem implements Serializable {

    @SerializedName("positionY")
    private Object positionY;

    @SerializedName("delay")
    private double delay;

    @SerializedName("name")
    private String name;

    @SerializedName("positionX")
    private Object positionX;

    @SerializedName("animationType")
    private String animationType;

    @SerializedName("haveAction")
    private List<ImageItem> haveAction;

    @SerializedName("tag")
    private String tag;

    @SerializedName("x1")
    private Object x1=null;

    @SerializedName("x2")
    private Object x2=null;

    @SerializedName("y1")
    private Object y1=null;

    @SerializedName("y2")
    private Object y2=null;

    public Object getX1() {
        return x1;
    }

    public void setX1(Object x1) {
        this.x1 = x1;
    }

    public Object getX2() {
        return x2;
    }

    public void setX2(Object x2) {
        this.x2 = x2;
    }

    public Object getY1() {
        return y1;
    }

    public void setY1(Object y1) {
        this.y1 = y1;
    }

    public Object getY2() {
        return y2;
    }

    public void setY2(Object y2) {
        this.y2 = y2;
    }

    public void setPositionY(Object positionY) {
        this.positionY = positionY;
    }

    public Object getPositionY() {
        return positionY;
    }

    public void setDelay(double delay) {
        this.delay = delay;
    }

    public double getDelay() {
        return delay;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPositionX(Object positionX) {
        this.positionX = positionX;
    }

    public Object getPositionX() {
        return positionX;
    }

    public void setAnimationType(String animationType) {
        this.animationType = animationType;
    }

    public String getAnimationType() {
        return animationType;
    }

    public void setHaveAction(List<ImageItem> haveAction) {
        this.haveAction = haveAction;
    }

    public List<ImageItem> getHaveAction() {
        return haveAction;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    @Override
    public String toString() {
        return
                "ImageItem{" +
                        "positionY = '" + positionY + '\'' +
                        ",delay = '" + delay + '\'' +
                        ",name = '" + name + '\'' +
                        ",positionX = '" + positionX + '\'' +
                        ",animationType = '" + animationType + '\'' +
                        ",haveAction = '" + haveAction + '\'' +
                        ",tag = '" + tag + '\'' +
                        "}";
    }
}