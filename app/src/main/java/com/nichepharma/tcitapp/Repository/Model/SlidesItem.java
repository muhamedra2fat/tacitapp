package com.nichepharma.tcitapp.Repository.Model;

import com.google.gson.annotations.SerializedName;

public class SlidesItem{

	@SerializedName("name")
	private String name;

	@SerializedName("_id")
	private String id;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"SlidesItem{" + 
			"name = '" + name + '\'' + 
			",_id = '" + id + '\'' + 
			"}";
		}
}