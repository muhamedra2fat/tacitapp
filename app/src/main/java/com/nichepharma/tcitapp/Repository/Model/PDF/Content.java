package com.nichepharma.tcitapp.Repository.Model.PDF;

import java.io.Serializable;

public class Content implements Serializable {

    private String ref;

    private String image;

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [ref = "+ref+", image = "+image+"]";
    }

}
