package com.nichepharma.tcitapp.Repository.Model.PDF;

import android.util.Log;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class PDFResponse implements Serializable{
    private List<Content> content;

    public List<Content> getContent ()
    {
        return content;
    }

    public void setContent (List<Content> content)
    {
        this.content = content;
    }

}
