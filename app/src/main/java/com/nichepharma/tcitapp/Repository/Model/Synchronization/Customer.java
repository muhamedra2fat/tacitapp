package com.nichepharma.tcitapp.Repository.Model.Synchronization;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class Customer implements Serializable {

	@SerializedName("customer_type")
	private String customerType;

	@SerializedName("speciality")
	private String speciality;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("class")
	private String classs;

	@SerializedName("doctors")
	private ArrayList<Customer> doctors;

	public void setCustomerType(String customerType){
		this.customerType = customerType;
	}

	public String getCustomerType(){
		return customerType;
	}

	public void setSpeciality(String speciality){
		this.speciality = speciality;
	}

	public String getSpeciality(){
		return speciality;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setclasss(String classs){
		this.classs = classs;
	}

	public String getclasss(){
		return classs;
	}

	public ArrayList<Customer> getDoctors() {
		return doctors;
	}

	public void setDoctors(ArrayList<Customer> doctors) {
		this.doctors = doctors;
	}

	@Override
 	public String toString(){
		return 
			"Customer{" + 
			"customer_type = '" + customerType + '\'' + 
			",speciality = '" + speciality + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",class = '" + classs + '\'' +
			"}";
		}
}