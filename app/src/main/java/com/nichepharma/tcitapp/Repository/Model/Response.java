package com.nichepharma.tcitapp.Repository.Model;

import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("company")
    private String company;

    @SerializedName("user")
    private User user;

    @SerializedName("token")
    private String token;

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompany() {
        return company;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return
                "Response{" +
                        "company = '" + company + '\'' +
                        ",user = '" + user + '\'' +
                        "}";
    }
}