package com.nichepharma.tcitapp.Repository.Retrofit;

import android.os.Build;

import com.nichepharma.tcitapp.Utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiFactory  {

    public static final String BASE_LINK = "https://tacitapp.herokuapp.com/api/";

    public static Retrofit getClient() {


        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .header("x-access-token", Constants.TOKEN)

                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient
                .readTimeout(60,TimeUnit.SECONDS)
                .connectTimeout(60,TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_LINK)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
    }
}