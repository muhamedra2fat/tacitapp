package com.nichepharma.tcitapp.Repository.Model.Synchronization;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class SlidesItem implements Serializable {

	@SerializedName("duration")
	private String duration;

	@SerializedName("name")
	private String name;

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"SlidesItem{" + 
			"duration = '" + duration + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}