package com.nichepharma.tcitapp.Repository.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class VersionsItem{

	@SerializedName("number")
	private int number;

	@SerializedName("_id")
	private String id;

	@SerializedName("sections")
	private List<SectionsItem> sections;

	public void setNumber(int number){
		this.number = number;
	}

	public int getNumber(){
		return number;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSections(List<SectionsItem> sections){
		this.sections = sections;
	}

	public List<SectionsItem> getSections(){
		return sections;
	}

	@Override
 	public String toString(){
		return 
			"VersionsItem{" + 
			"number = '" + number + '\'' + 
			",_id = '" + id + '\'' + 
			",sections = '" + sections + '\'' + 
			"}";
		}
}