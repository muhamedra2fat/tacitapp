package com.nichepharma.tcitapp.Repository.Model;

import com.google.gson.annotations.SerializedName;

public class CompanyLogoItem{

	@SerializedName("xPostion")
	private int xPostion;

	@SerializedName("imgPath")
	private String imgPath;

	@SerializedName("width")
	private int width;

	@SerializedName("yPostion")
	private int yPostion;

	@SerializedName("height")
	private int height;

	public void setXPostion(int xPostion){
		this.xPostion = xPostion;
	}

	public int getXPostion(){
		return xPostion;
	}

	public void setImgPath(String imgPath){
		this.imgPath = imgPath;
	}

	public String getImgPath(){
		return imgPath;
	}

	public void setWidth(int width){
		this.width = width;
	}

	public int getWidth(){
		return width;
	}

	public void setYPostion(int yPostion){
		this.yPostion = yPostion;
	}

	public int getYPostion(){
		return yPostion;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}

	@Override
 	public String toString(){
		return 
			"CompanyLogoItem{" + 
			"xPostion = '" + xPostion + '\'' + 
			",imgPath = '" + imgPath + '\'' + 
			",width = '" + width + '\'' + 
			",yPostion = '" + yPostion + '\'' + 
			",height = '" + height + '\'' + 
			"}";
		}
}