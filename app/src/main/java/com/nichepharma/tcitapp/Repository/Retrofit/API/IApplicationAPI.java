package com.nichepharma.tcitapp.Repository.Retrofit.API;

import com.nichepharma.tcitapp.Repository.Model.LoginResponse;
import com.nichepharma.tcitapp.Repository.Model.ProductsResponse;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.SynchronizationResponse;
import com.nichepharma.tcitapp.Repository.Model.UserCustomersResponse;
import com.nichepharma.tcitapp.Repository.Model.Versions.VersionResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface IApplicationAPI {

    @FormUrlEncoded
    @POST("/companies/login")
    Call<LoginResponse> login(@Field("username") String username,
                              @Field("password") String password);

    @GET("companies/get/customers/by-user/{companyId}/{userId}")
    Call<UserCustomersResponse> getUserCustomers(@Path("companyId") String companyId,
                                                 @Path("userId") String userId);

    @GET("products/get-versions/{companyId}")
    Call<VersionResponse> getVersions(@Path("companyId") String companyId);

    @GET("products/get/{companyId}")
    Call<ProductsResponse> getProducts(@Path("companyId") String companyId);


    @POST("visits/create")
    Call<SynchronizationResponse> sendVisitsAndCalls(@Body Synchronization synchronization);

    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);
}
