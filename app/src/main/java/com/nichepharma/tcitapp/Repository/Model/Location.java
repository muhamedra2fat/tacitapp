package com.nichepharma.tcitapp.Repository.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Location implements Serializable {

	@SerializedName("country")
	private String country;

	@SerializedName("province")
	private String province;

	@SerializedName("city")
	private String city;

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setProvince(String province){
		this.province = province;
	}

	public String getProvince(){
		return province;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	@Override
 	public String toString(){
		return 
			"Location{" + 
			"country = '" + country + '\'' + 
			",province = '" + province + '\'' + 
			",city = '" + city + '\'' + 
			"}";
		}
}