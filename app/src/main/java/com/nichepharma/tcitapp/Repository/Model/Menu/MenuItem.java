package com.nichepharma.tcitapp.Repository.Model.Menu;


import com.google.gson.annotations.SerializedName;


public class MenuItem {

	@SerializedName("image")
	private String image;

	@SerializedName("goTo")
	private String goTo;

	@SerializedName("menuX")
	private String menuX;

	@SerializedName("menuY")
	private String menuY;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setGoTo(String goTo){
		this.goTo = goTo;
	}

	public String getGoTo(){
		return goTo;
	}

	public void setMenuX(String menuX){
		this.menuX = menuX;
	}

	public String getMenuX(){
		return menuX;
	}

	public void setMenuY(String menuY){
		this.menuY = menuY;
	}

	public String getMenuY(){
		return menuY;
	}

	@Override
 	public String toString(){
		return 
			"ContentItem{" + 
			"image = '" + image + '\'' + 
			",goTo = '" + goTo + '\'' + 
			",menuX = '" + menuX + '\'' + 
			",menuY = '" + menuY + '\'' + 
			"}";
		}
}