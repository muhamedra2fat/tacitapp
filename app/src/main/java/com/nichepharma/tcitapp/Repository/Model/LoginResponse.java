package com.nichepharma.tcitapp.Repository.Model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse{

	@SerializedName("response")
	private Response response;

	@SerializedName("status")
	private String status;

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"LoginResponse{" +
			"response = '" + response + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}