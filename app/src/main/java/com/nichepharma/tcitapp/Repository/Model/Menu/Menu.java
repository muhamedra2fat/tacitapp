package com.nichepharma.tcitapp.Repository.Model.Menu;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Menu{

	@SerializedName("menuButton")
	private String menuButton;

	@SerializedName("menuBKG")
	private String menuBKG;

	@SerializedName("content")
	private List<MenuItem> content;

	public void setMenuButton(String menuButton){
		this.menuButton = menuButton;
	}

	public String getMenuButton(){
		return menuButton;
	}

	public void setMenuBKG(String menuBKG){
		this.menuBKG = menuBKG;
	}

	public String getMenuBKG(){
		return menuBKG;
	}

	public void setContent(List<MenuItem> content){
		this.content = content;
	}

	public List<MenuItem> getContent(){
		return content;
	}

	@Override
 	public String toString(){
		return 
			"Menu{" + 
			"menuButton = '" + menuButton + '\'' + 
			",menuBKG = '" + menuBKG + '\'' + 
			",content = '" + content + '\'' + 
			"}";
		}
}