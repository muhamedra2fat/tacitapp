package com.nichepharma.tcitapp.Repository.Model;

 public class CompanyLogo {
    private String imgPath;
    private float xPostion;
    private float yPostion;
    private float width;
    private float height;


    // Getter Methods

    public String getImgPath() {
        return imgPath;
    }

    public float getXPostion() {
        return xPostion;
    }

    public float getYPostion() {
        return yPostion;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    // Setter Methods

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public void setXPostion(float xPostion) {
        this.xPostion = xPostion;
    }

    public void setYPostion(float yPostion) {
        this.yPostion = yPostion;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }
}