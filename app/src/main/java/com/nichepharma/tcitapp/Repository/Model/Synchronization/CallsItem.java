package com.nichepharma.tcitapp.Repository.Model.Synchronization;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CallsItem implements Serializable {

	@SerializedName("duration")
	private long duration;

	@SerializedName("product")
	private Product product;

	@SerializedName("start_timestamp")
	private String startTimestamp;

	@SerializedName("call_objection")
	private String callObjection;

	@SerializedName("_id")
	private String id;

	@SerializedName("call_remark")
	private String callRemark;

	@SerializedName("sample_type")
	private String sampleType;

	@SerializedName("samples")
	private int samples;

	public void setDuration(long duration){
		this.duration = duration;
	}

	public long getDuration(){
		return duration;
	}

	public void setProduct(Product product){
		this.product = product;
	}

	public Product getProduct(){
		return product;
	}

	public void setStartTimestamp(String startTimestamp){
		this.startTimestamp = startTimestamp;
	}

	public String getStartTimestamp(){
		return startTimestamp;
	}

	public void setCallObjection(String callObjection){
		this.callObjection = callObjection;
	}

	public String getCallObjection(){
		return callObjection;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCallRemark(String callRemark){
		this.callRemark = callRemark;
	}

	public String getCallRemark(){
		return callRemark;
	}

	public void setSampleType(String sampleType){
		this.sampleType = sampleType;
	}

	public String getSampleType(){
		return sampleType;
	}

	public void setSamples(int samples){
		this.samples = samples;
	}

	public int getSamples(){
		return samples;
	}

	@Override
 	public String toString(){
		return 
			"CallsItem{" + 
			"duration = '" + duration + '\'' + 
			",product = '" + product + '\'' + 
			",start_timestamp = '" + startTimestamp + '\'' + 
			",call_objection = '" + callObjection + '\'' + 
			",_id = '" + id + '\'' + 
			",call_remark = '" + callRemark + '\'' + 
			",sample_type = '" + sampleType + '\'' + 
			",samples = '" + samples + '\'' + 
			"}";
		}
}