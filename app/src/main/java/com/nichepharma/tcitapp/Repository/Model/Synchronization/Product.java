package com.nichepharma.tcitapp.Repository.Model.Synchronization;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Product implements Serializable {

	@SerializedName("slides")
	private List<SlidesItem> slides;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("version")
	private double version;

	public void setSlides(List<SlidesItem> slides){
		this.slides = slides;
	}

	public List<SlidesItem> getSlides(){
		return slides;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setVersion(double version){
		this.version = version;
	}

	public double getVersion(){
		return version;
	}

	@Override
 	public String toString(){
		return 
			"Product{" + 
			"slides = '" + slides + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",version = '" + version + '\'' + 
			"}";
		}
}