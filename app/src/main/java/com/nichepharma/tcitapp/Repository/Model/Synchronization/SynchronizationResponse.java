package com.nichepharma.tcitapp.Repository.Model.Synchronization;

import com.google.gson.annotations.SerializedName;

public class SynchronizationResponse {

    @SerializedName("status")
    private String status;


    @SerializedName("response")
    private Synchronization response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Synchronization getResponse() {
        return response;
    }

    public void setResponse(Synchronization response) {
        this.response = response;
    }
}
