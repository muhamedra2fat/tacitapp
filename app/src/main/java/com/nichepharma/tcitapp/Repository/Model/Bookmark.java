package com.nichepharma.tcitapp.Repository.Model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Bookmark implements Serializable {

	@SerializedName("slideshows")
	private List<SlideshowsItem> slideshows;

	@SerializedName("appID")
	private String appID;

	@SerializedName("name")
	private String name;

	@SerializedName("companyLogo")
	private List<CompanyLogo> companyLogos;

	public void setSlideshows(List<SlideshowsItem> slideshows){
		this.slideshows = slideshows;
	}

	public List<SlideshowsItem> getSlideshows(){
		return slideshows;
	}

	public void setAppID(String appID){
		this.appID = appID;
	}

	public String getAppID(){
		return appID;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public List<CompanyLogo> getCompanyLogo() {
		return companyLogos;
	}

	public void setCompanyLogo(List<CompanyLogo> companyLogos) {
		this.companyLogos = companyLogos;
	}

	@Override
 	public String toString(){
		return 
			"Bookmark{" + 
			"slideshows = '" + slideshows + '\'' + 
			",appID = '" + appID + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}