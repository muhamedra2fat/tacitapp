package com.nichepharma.tcitapp.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nichepharma.tcitapp.Repository.Model.Customer;
import com.nichepharma.tcitapp.Repository.Model.Product;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.CallsItem;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.SlidesItem;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.SynchronizationResponse;
import com.nichepharma.tcitapp.Repository.Model.User;
import com.nichepharma.tcitapp.Repository.Retrofit.API.ApplicationAPI;
import com.nichepharma.tcitapp.Repository.Retrofit.ApiFactory;
import com.nichepharma.tcitapp.Repository.SharedPreference.MySharedPreferenceHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SychronizationViewModel extends AndroidViewModel {
    private MySharedPreferenceHelper mySharedPreferenceHelper;
    private LiveData<SynchronizationResponse> synchronizationResponseLiveData;
    private final static String SYNCHRONIZATION_SP = "sync_sp";
    private final static String SYNCHRONIZATION_LIST = "sync_list";
    private static Synchronization sychronization;
    private LoginViewModel loginViewModel;
    private ApplicationAPI applicationAPI;

    public SychronizationViewModel(@NonNull Application application) {
        super(application);
        mySharedPreferenceHelper = new MySharedPreferenceHelper(application.getSharedPreferences(SYNCHRONIZATION_SP, Context.MODE_PRIVATE));
        loginViewModel = new LoginViewModel(application);
        applicationAPI = new ApplicationAPI();
    }


    public void sendVisitsAndCalls(Synchronization synchronization) {
        synchronizationResponseLiveData = applicationAPI.sendVisitsAndCalls(synchronization);
    }

    public void deleteVisitsAndCalls() {
        mySharedPreferenceHelper.clearSharedPreference();
    }

    public LiveData<SynchronizationResponse> getSynchronizationResponseLiveData() {
        return synchronizationResponseLiveData;
    }


    public Synchronization setSychronizationCustomerAndLocation(Customer customer, Synchronization sychronization , ArrayList<Customer> doctors) {
        com.nichepharma.tcitapp.Repository.Model.Synchronization.Customer customer1 = new com.nichepharma.tcitapp.Repository.Model.Synchronization.Customer();
        customer1.setId(customer.getId());
        customer1.setName(customer.getName());
        customer1.setCustomerType(customer.getType());
        customer1.setSpeciality(customer.getSpeciality());
        customer1.setclasss(customer.getClasss());

        ArrayList<com.nichepharma.tcitapp.Repository.Model.Synchronization.Customer> doctorsArrayList = new ArrayList<>();
        for (int i = 0; i < doctors.size(); i++) {
            com.nichepharma.tcitapp.Repository.Model.Synchronization.Customer tempDoctor = new com.nichepharma.tcitapp.Repository.Model.Synchronization.Customer();
            tempDoctor.setId(doctors.get(i).getId());
            tempDoctor.setName(doctors.get(i).getName());
            tempDoctor.setCustomerType(doctors.get(i).getType());
            tempDoctor.setSpeciality(doctors.get(i).getSpeciality());
            tempDoctor.setclasss(doctors.get(i).getClasss());
            doctorsArrayList.add(tempDoctor);

        }

        customer1.setDoctors(doctorsArrayList);
        sychronization.setCustomer(customer1);
        sychronization.setLocation(customer.getLocation());
        return sychronization;
    }


    public void setSychronizationUser() {
        User user = loginViewModel.getUserInfoFromSharedPreference();
        com.nichepharma.tcitapp.Repository.Model.Synchronization.User user1 = new com.nichepharma.tcitapp.Repository.Model.Synchronization.User();
        user1.setId(user.getId());
        user1.setName(user.getName());

        sychronization.setUser(user1);
        sychronization.setCompanyId(loginViewModel.getCompanyIdFromSharedPreference());

    }

    public Synchronization addProduct(Product product, HashMap<Integer, Long> duration, String startStamp) {
        if (sychronization == null) {
            sychronization = new Synchronization();
            setSychronizationUser();
        }
        com.nichepharma.tcitapp.Repository.Model.Synchronization.Product product1 = new com.nichepharma.tcitapp.Repository.Model.Synchronization.Product();
        List<SlidesItem> slidesItems = new ArrayList<>();
        long totalDuration = 0;
        for (int i = 0; i < duration.size(); i++) {
            totalDuration += duration.get(i);
            SlidesItem slidesItem = new SlidesItem();
            slidesItem.setName(String.valueOf(i + 1));
            slidesItem.setDuration(String.valueOf(duration.get(i)));
            slidesItems.add(slidesItem);
        }
        product1.setSlides(slidesItems);
        product1.setId(product.getId());
        product1.setName(product.getName());
        product1.setVersion(product.getV());

        CallsItem callsItem = new CallsItem();
        callsItem.setDuration(totalDuration);
        callsItem.setProduct(product1);
        callsItem.setStartTimestamp(startStamp);

        List<CallsItem> callsItemList = sychronization.getCalls();
        callsItemList.add(callsItem);

        sychronization.setCalls(callsItemList);
        return sychronization;
    }

    public void saveSynchronizationSession() {
        if (sychronization != null) {
            addSychronizationObjectToSharedPreference(sychronization);
            sychronization = null;
        }

    }


    public void updateSynchronization(Synchronization synchronization) {
        List<Synchronization> synchronizationList = getSynchronizationList();
        for (int i = 0; i < synchronizationList.size(); i++) {
            if (synchronizationList.get(i).getId().equals(synchronization.getId())) {
                synchronizationList.remove(i);
                synchronizationList.add(synchronization);
                break;
            }
        }
        saveSynchronizationList(synchronizationList);
    }

    public void addSychronizationObjectToSharedPreference(Synchronization sychronization) {
        List<Synchronization> synchronizationList = getSynchronizationList();
        synchronizationList.add(sychronization);
        saveSynchronizationList(synchronizationList);

    }

    public void saveSynchronizationList(List<Synchronization> synchronizationList) {
        String json = new Gson().toJson(synchronizationList);
        mySharedPreferenceHelper.saveToSharedPreference(SYNCHRONIZATION_LIST, json);
    }

    public List<Synchronization> getSynchronizationList() {
        String jsonSyncList = mySharedPreferenceHelper.getFromSharedPreference(SYNCHRONIZATION_LIST, "");
        if (jsonSyncList.isEmpty())
            return new ArrayList<>();
        Type type = new TypeToken<List<Synchronization>>() {
        }.getType();

        return new Gson().fromJson(jsonSyncList, type);
    }
}
