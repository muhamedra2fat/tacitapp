package com.nichepharma.tcitapp.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nichepharma.tcitapp.Repository.Model.Product;
import com.nichepharma.tcitapp.Repository.Model.ProductsResponse;
import com.nichepharma.tcitapp.Repository.Model.Versions.VersionResponse;
import com.nichepharma.tcitapp.Repository.Retrofit.API.ApplicationAPI;
import com.nichepharma.tcitapp.Repository.SharedPreference.MySharedPreferenceHelper;
import com.nichepharma.tcitapp.Utils.Constants;

import java.lang.reflect.Type;
import java.util.List;

import okhttp3.ResponseBody;

public class ProductViewModel extends AndroidViewModel {

    private ApplicationAPI applicationAPI;
    private LiveData<ProductsResponse> productsLiveData;
    private LiveData<ResponseBody> downloadFileLiveData;
    private LiveData<VersionResponse> getVersionsLiveData;
    private MySharedPreferenceHelper sharedPreferenceHelper;
    private final static String PRODUCT_PREFS = "product_prefs";
    private final static String PRODUCTS = "products";

    public ProductViewModel(Application application) {
        super(application);
        applicationAPI = new ApplicationAPI();
        sharedPreferenceHelper = new MySharedPreferenceHelper(
                application.getSharedPreferences(PRODUCT_PREFS, Context.MODE_PRIVATE)
        );
    }

    public void getProducts() {
        productsLiveData = applicationAPI.getProducts(Constants.COMPANY_ID);
    }

    public void getGetVersion() {
        getVersionsLiveData = applicationAPI.getVersions(Constants.COMPANY_ID);
    }

    public void saveProductsIntoSharedPreference(List<Product> product) {
        String json = new Gson().toJson(product);
        sharedPreferenceHelper.saveToSharedPreference(PRODUCTS, json);
    }

    public void updateProductFromSharedPreferences(int position, Product product) {
        List<Product> productList = getProductsFromSharedPrefernce();
        productList.remove(position);
        productList.add(position, product);
        saveProductsIntoSharedPreference(productList);
    }

    public void deleteProducts() {
        sharedPreferenceHelper.clearSharedPreference();
    }

    public List<Product> getProductsFromSharedPrefernce() {
        String json = sharedPreferenceHelper.getFromSharedPreference(PRODUCTS, "");
        if (json.isEmpty())
            return null;
        Type type = new TypeToken<List<Product>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public LiveData<ProductsResponse> getProductsLiveData() {
        return productsLiveData;
    }

    public LiveData<VersionResponse> getGetVersionsLiveData() {
        return getVersionsLiveData;
    }

    public void downloadFile(String url) {
        downloadFileLiveData = applicationAPI.downloadFile(url);
    }

    public LiveData<ResponseBody> getDownloadFileLiveData() {
        return downloadFileLiveData;
    }
}
