package com.nichepharma.tcitapp.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nichepharma.tcitapp.Repository.Model.Customer;
import com.nichepharma.tcitapp.Repository.Model.UserCustomersResponse;
import com.nichepharma.tcitapp.Repository.Retrofit.API.ApplicationAPI;
import com.nichepharma.tcitapp.Repository.SharedPreference.MySharedPreferenceHelper;
import com.nichepharma.tcitapp.Utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomersViewModel extends AndroidViewModel {
    private ApplicationAPI applicationAPI;
    private LiveData<UserCustomersResponse> userCustomersLiveData;
    private final static String USER_CUTOMERS = "user_customers";
    private final static String USER_CUTOMERS_SP = "user_customers_sp";
    private MySharedPreferenceHelper sharedPreferenceHelper;

    public CustomersViewModel(Application application) {
        super(application);

        applicationAPI = new ApplicationAPI();
        sharedPreferenceHelper = new MySharedPreferenceHelper
                (application.getSharedPreferences(USER_CUTOMERS_SP, Context.MODE_PRIVATE));
    }

    public void getUserCustomers() {
        userCustomersLiveData = applicationAPI.getUserCustomers(Constants.COMPANY_ID, Constants.USER_ID);
    }


    public LiveData<UserCustomersResponse> getUserCustomersLiveData() {
        return userCustomersLiveData;
    }

    public void deleteCustomers() {
        sharedPreferenceHelper.clearSharedPreference();
    }


    public void addCustomersToSharedPreference(HashMap<String, ArrayList<Customer>> map) {
        Gson gson = new Gson();
        String hashMapString = gson.toJson(map);
        sharedPreferenceHelper.saveToSharedPreference(USER_CUTOMERS, hashMapString);
    }

    public HashMap<String, ArrayList<Customer>> getCustomersMap() {
        String mapJson = sharedPreferenceHelper.getFromSharedPreference(USER_CUTOMERS, null);
        java.lang.reflect.Type type = new TypeToken<HashMap<String, ArrayList<Customer>>>() {
        }.getType();
        HashMap<String, ArrayList<Customer>> testHashMap2 = new Gson().fromJson(mapJson, type);
        return testHashMap2;
    }
}
