package com.nichepharma.tcitapp.ViewModel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;

import com.google.gson.Gson;
import com.nichepharma.tcitapp.Repository.Model.LoginResponse;
import com.nichepharma.tcitapp.Repository.Model.User;
import com.nichepharma.tcitapp.Repository.Retrofit.API.ApplicationAPI;
import com.nichepharma.tcitapp.Repository.SharedPreference.MySharedPreferenceHelper;

public class LoginViewModel extends AndroidViewModel {

    private ApplicationAPI applicationAPI;
    private LiveData<LoginResponse> loginResponseLiveData;
    private MySharedPreferenceHelper sharedPreferenceHelper;
    private final static String USER_SHARED_PREF = "user_prefs";
    private final static String USER_INFO = "user_info";
    private final static String COMPANY_ID = "company_id";
    private final static String TOKEN = "token";

    public LoginViewModel(Application application) {
        super(application);
        applicationAPI = new ApplicationAPI();
        sharedPreferenceHelper = new MySharedPreferenceHelper(
                application.getSharedPreferences(USER_SHARED_PREF, Context.MODE_PRIVATE)
        );
    }

    public void login(String username, String password) {
        loginResponseLiveData = applicationAPI.login(username, password);
    }

    public void logout() {
        sharedPreferenceHelper.clearSharedPreference();
    }

    public void addUserInfoToSharedPreference(User user) {
        String json = new Gson().toJson(user);
        sharedPreferenceHelper.saveToSharedPreference(USER_INFO, json);
    }

    public void addCompanyIdToSharedPreference(String companyId) {
        sharedPreferenceHelper.saveToSharedPreference(COMPANY_ID, companyId);
    }

    public void addTokenToSharedPreference(String token) {
        sharedPreferenceHelper.saveToSharedPreference(TOKEN, token);
    }

    public User getUserInfoFromSharedPreference() {
        String userJson = sharedPreferenceHelper.getFromSharedPreference(USER_INFO, "");
        if (userJson.isEmpty())
            return null;
        return new Gson().fromJson(userJson, User.class);
    }

    public String getCompanyIdFromSharedPreference() {
        return sharedPreferenceHelper.getFromSharedPreference(COMPANY_ID, "");
    }

    public String getTokenFromSharedPreference() {
        return sharedPreferenceHelper.getFromSharedPreference(TOKEN, "");
    }

    public LiveData<LoginResponse> getLoginResponseLiveData() {
        return loginResponseLiveData;
    }
}
