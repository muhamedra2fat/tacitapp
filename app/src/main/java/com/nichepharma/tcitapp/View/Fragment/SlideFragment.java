package com.nichepharma.tcitapp.View.Fragment;

import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.ImageItem;
import com.nichepharma.tcitapp.Repository.Model.SlideshowsItem;
import com.nichepharma.tcitapp.Utils.Constants;

import java.io.File;
import java.util.List;

public class SlideFragment extends Fragment {
    private RelativeLayout relativelayout;
    private String externalPath = "";

    private SlideshowsItem getSlideShowItem() {
        return (SlideshowsItem) getArguments().getSerializable("slide");
    }

    private String getProductName() {
        return getArguments().getString(Constants.PRODUCT_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slide, container, false);
        animateSlides(view);
        externalPath = getContext().getExternalFilesDir(null).getPath();
        return view;
    }


    private void animateSlides(View view) {

        final SlideshowsItem slideshowsItem = getSlideShowItem();
        final List<ImageItem> images = slideshowsItem.getImage();


        relativelayout = view.findViewById(R.id.container_relativeLayout2);

        relativelayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        String path = externalPath + File.separator + getProductName()
                                + File.separator + slideshowsItem.getSlideBKG();
                        Drawable d = Drawable.createFromPath(path);
                        relativelayout.setBackground(d);

                        for (int j = 0; j < images.size(); j++) {
                            final ImageItem imageItem = images.get(j);

                            if (imageItem != null) {
                                animate(imageItem);
                                if (imageItem.getHaveAction() != null && imageItem.getHaveAction().size() > 0)
                                    for (int i = 0; i < imageItem.getHaveAction().size(); i++) {
                                        animate(imageItem.getHaveAction().get(i));
                                    }
                            }

                            relativelayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                        }
                    }
                });

    }

    private float getPosition(String position, int imageWidthOrHeight) {
        float pos;
        if (position.equals("setXCenter"))
            pos = (float) (relativelayout.getWidth() / 2.0) - (imageWidthOrHeight / 2) ;
        else if (position.equals("setYCenter"))
            pos = (float) (relativelayout.getHeight() / 2.0) - (imageWidthOrHeight / 2);
        else
            pos = Float.parseFloat(position.toString());

        return pos ;

    }

    private void animate(ImageItem imageItem) {

        float x = 0, y = 0, x2 = 0, y2 = 0;
        if (imageItem == null)
            return;
        String path = externalPath + File.separator + getProductName()
                + File.separator + (imageItem.getName().contains(".png") ? imageItem.getName() : imageItem.getName() + ".png");
        File imgFile = new File(path);

        ImageView imageview = new ImageView(getContext());

        Drawable d = Drawable.createFromPath(path);

        imageview.setBackground(d);
        if (imageview.getParent() != null)
            ((ViewGroup) imageview.getParent()).removeView(imageview);


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);

        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;


        if (imageItem.getPositionY() == null || imageItem.getPositionX() == null) {
            if (imageItem.getX1() == null || imageItem.getX2() == null || imageItem.getY1() == null || imageItem.getY2() == null) {
                return;
            } else {
                x = getPosition(imageItem.getX1().toString() , imageWidth);
                y = getPosition(imageItem.getY1().toString(), imageHeight);
                x2 = getPosition(imageItem.getX2().toString(), imageWidth);
                y2 = getPosition(imageItem.getY2().toString(), imageHeight);

            }
        } else {
            x = getPosition(imageItem.getPositionX().toString(), imageWidth);
            y = getPosition(imageItem.getPositionY().toString(), imageHeight);
        }

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(imageWidth, imageHeight);

        layoutParams.leftMargin = (int) x;
        layoutParams.topMargin = (int) y;
        relativelayout.addView(imageview, layoutParams);


        if (imageItem.getAnimationType().equals("setFrameWithPostion") || imageItem.getAnimationType().equals("setFrameWithPostion"))
            imageview.animate().x(x2).y(y2).setDuration(imageItem.getDelay() == 0 ? 2000 : (long) imageItem.getDelay() * 1000);
        else {
            getAnimation(imageItem.getAnimationType(), imageview, (long) imageItem.getDelay());
        }

        imageview.requestLayout();

    }

    private void getAnimation(String type, final ImageView imageview, long delay) {
        android.view.animation.Animation animSlide;
        delay = delay == 0 ? 2000 : delay * 1000;

        switch (type) {
            case "setFadeAnimations":
                animSlide = AnimationUtils.loadAnimation(getContext(),
                        android.support.v7.appcompat.R.anim.abc_fade_in);
                animSlide.setDuration(delay);
                imageview.setAnimation(animSlide);
                imageview.startAnimation(animSlide);
                break;
//            case "setBarAnimations":
//                imageview.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                final int targetHeight = imageview.getMeasuredHeight();
//
//                // Older versions of android (pre API 21) cancel animations for views with a height of 0.
//                imageview.getLayoutParams().height = 1;
//                imageview.setVisibility(View.VISIBLE);
//                Animation a = new Animation() {
//                    @Override
//                    protected void applyTransformation(float interpolatedTime, Transformation t) {
//                        t.setTransformationType(Transformation.TYPE_ALPHA);
//                        imageview.getLayoutParams().height = interpolatedTime == 1
//                                ? RelativeLayout.LayoutParams.WRAP_CONTENT
//                                : (int) (targetHeight * interpolatedTime);
//                        imageview.requestLayout();
//
//                    }
//
//                    @Override
//                    public boolean willChangeBounds() {
//                        return true;
//                    }
//                };
//
//                // 1dp/ms
//                a.setDuration(delay);
//                imageview.startAnimation(a);
//
//                break;
            case "setDropeAnimations":
                animSlide = AnimationUtils.loadAnimation(getContext(),
                        android.support.v7.appcompat.R.anim.abc_slide_in_top);
                animSlide.setDuration(delay);
                imageview.setAnimation(animSlide);
                imageview.startAnimation(animSlide);
                break;
            case "setWidthAnimations":
                animSlide = AnimationUtils.loadAnimation(getContext(), R.anim.slide_from_left);
                animSlide.setDuration(delay);
                imageview.setAnimation(animSlide);
                imageview.startAnimation(animSlide);
                break;
//            case "setWidthBarAnimations":
//                imageview.measure(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//                final int targetWidth = imageview.getMeasuredWidth();
//
//                // Older versions of android (pre API 21) cancel animations for views with a height of 0.
//                imageview.getLayoutParams().width = 1;
//                imageview.setVisibility(View.VISIBLE);
//                Animation a2 = new Animation() {
//                    @Override
//                    protected void applyTransformation(float interpolatedTime, Transformation t) {
//                        t.setTransformationType(Transformation.TYPE_ALPHA);
//                        imageview.getLayoutParams().width = interpolatedTime == 1
//                                ? RelativeLayout.LayoutParams.WRAP_CONTENT
//                                : (int) (targetWidth * interpolatedTime);
//                        imageview.requestLayout();
//                    }
//
//                    @Override
//                    public boolean willChangeBounds() {
//                        return true;
//                    }
//                };
//
//                // 1dp/ms
//                a2.setDuration(delay);
//                imageview.startAnimation(a2);
//
//
//                break;
            default:
                animSlide = AnimationUtils.loadAnimation(getContext(),
                        android.support.v7.appcompat.R.anim.abc_fade_in);
                animSlide.setDuration(delay);
                imageview.setAnimation(animSlide);
                imageview.startAnimation(animSlide);
                break;
        }

    }
}
