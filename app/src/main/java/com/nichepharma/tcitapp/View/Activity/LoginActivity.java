package com.nichepharma.tcitapp.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.LoginResponse;
import com.nichepharma.tcitapp.Repository.Model.User;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.ViewModel.LoginViewModel;
import com.nichepharma.tcitapp.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityLoginBinding activityLoginBinding;
    private LoginViewModel loginViewModel;

    private void init() {
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);

    }

    private boolean checkUserData() {
        String companyID = loginViewModel.getCompanyIdFromSharedPreference();
        User user = loginViewModel.getUserInfoFromSharedPreference();
        String token = loginViewModel.getTokenFromSharedPreference();
        if (user == null || companyID.isEmpty() || token.isEmpty())
            return false;
        Constants.COMPANY_ID = companyID;
        Constants.USER_ID = user.getId();
        Constants.TOKEN = token;
        return true;
    }

    private void setListeners() {
        activityLoginBinding.loginImageView.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        init();
        if (checkUserData())
            transferToNextActivity();

        setListeners();


    }

    private void observeViewModel() {
        loginViewModel.getLoginResponseLiveData().observe(this, new Observer<LoginResponse>() {
            @Override
            public void onChanged(@Nullable LoginResponse loginResponse) {
                activityLoginBinding.loginProgressBar.setVisibility(View.GONE);
                if (loginResponse != null) {
                    if (loginResponse.getStatus().equals("success")) {

                        loginViewModel.addUserInfoToSharedPreference(loginResponse.getResponse().getUser());
                        loginViewModel.addCompanyIdToSharedPreference(loginResponse.getResponse().getCompany());
                        loginViewModel.addTokenToSharedPreference(loginResponse.getResponse().getToken());

                        Constants.COMPANY_ID = loginResponse.getResponse().getCompany();
                        Constants.USER_ID = loginResponse.getResponse().getUser().getId();
                        Constants.TOKEN = loginResponse.getResponse().getToken();

                        transferToNextActivity();
                    } else
                        Toast.makeText(LoginActivity.this, "Wrong username or password", Toast.LENGTH_SHORT).show();

                }
                activityLoginBinding.loginImageView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void transferToNextActivity() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        LoginActivity.this.finish();
    }

    private boolean validateUsername() {
        if (activityLoginBinding.usernameEditText.getText().toString().isEmpty()) {
            activityLoginBinding.usernameEditText.setError("Required");
            return false;
        }
        return true;
    }

    private boolean validatePassword() {
        if (activityLoginBinding.passwordEditText.getText().toString().isEmpty()) {
            activityLoginBinding.passwordEditText.setError("Required");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_imageView:
                if (validateUsername() && validatePassword()) {
                    activityLoginBinding.loginProgressBar.setVisibility(View.VISIBLE);
                    activityLoginBinding.loginImageView.setVisibility(View.GONE);
                    loginViewModel.login(activityLoginBinding.usernameEditText.getText().toString(),
                            activityLoginBinding.passwordEditText.getText().toString());
                    observeViewModel();
                }
                break;
        }
    }
}
