package com.nichepharma.tcitapp.View.Activity;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.CallsItem;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.ViewModel.SychronizationViewModel;
import com.nichepharma.tcitapp.databinding.FragmentFeedbackBinding;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener {
    private FragmentFeedbackBinding fragmentFeedbackBinding;
    private SychronizationViewModel sychronizationViewModel;

    private void initSpinner() {
        String ones[] = {"ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT",
                "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN",
                "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN",
                "EIGHTEEN", "NINETEEN", "TWENTY"};
        List<String> dataset = new LinkedList<>(Arrays.asList(ones));
        fragmentFeedbackBinding.sampleDropSpinner.attachDataSource(dataset);

    }

    private Synchronization getSynchronization() {
        return (Synchronization) getIntent().getExtras().getSerializable(Constants.SYNCHRONIZATION_OBJ);
    }

    private int getPosition() {
        return getIntent().getExtras().getInt(Constants.SYNC_POSITION);
    }


    private void setListeners() {
        fragmentFeedbackBinding.submitImageView.setOnClickListener(this);
        fragmentFeedbackBinding.closeImageView.setOnClickListener(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentFeedbackBinding = DataBindingUtil.setContentView(this, R.layout.fragment_feedback);
        sychronizationViewModel = ViewModelProviders.of(this).get(SychronizationViewModel.class);
        initSpinner();
        setListeners();

    }


    private void updateSynchronizationObject() {
        Synchronization synchronization = getSynchronization();
        int index = getPosition();
        if (getPosition() <= 0)
            index = getProductDoNotHaveFeedBackIndex(synchronization);
        if (index != -1) {
            CallsItem callsItem = synchronization.getCalls().get(index);
            callsItem.setCallObjection(fragmentFeedbackBinding.callObjectionEditText.getText().toString());
            callsItem.setCallRemark(fragmentFeedbackBinding.callRemarkEditText.getText().toString());
            callsItem.setSamples(fragmentFeedbackBinding.sampleDropSpinner.getSelectedIndex() + 1);
            synchronization.getCalls().set(index, callsItem);

            sychronizationViewModel.updateSynchronization(synchronization);

        }


    }

    private int getProductDoNotHaveFeedBackIndex(Synchronization synchronization) {
        for (int i = 0; i < synchronization.getCalls().size(); i++) {
            CallsItem callsItem = synchronization.getCalls().get(i);
            if (callsItem.getCallObjection() == null || callsItem.getCallRemark() == null || callsItem.getSamples() <= 0)
                return i;
        }
        return -1;
    }

    private boolean validateFields() {
        boolean isValid = true;
        if (fragmentFeedbackBinding.callObjectionEditText.getText().toString().isEmpty()) {
            fragmentFeedbackBinding.callObjectionEditText.setError("Required");
            isValid = false;
        }
        if (fragmentFeedbackBinding.callRemarkEditText.getText().toString().isEmpty()) {
            fragmentFeedbackBinding.callRemarkEditText.setError("Required");
            isValid = false;
        }
        return isValid;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_imageView:
                if (validateFields()) {
                    updateSynchronizationObject();
                    startActivity(new Intent(FeedbackActivity.this, SychronizationActivity.class));
                    finish();
                }
                break;
            case R.id.close_imageView:
                onBackPressed();
                break;
        }
    }
}
