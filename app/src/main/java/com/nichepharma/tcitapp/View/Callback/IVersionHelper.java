package com.nichepharma.tcitapp.View.Callback;

public interface IVersionHelper {

    double getCurrentVersion(String productId);

    void updateProduct(String productId , double version);
}
