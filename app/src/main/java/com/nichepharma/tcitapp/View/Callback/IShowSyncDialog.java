package com.nichepharma.tcitapp.View.Callback;

import android.view.View;

import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;

public interface IShowSyncDialog {

    void showSyncDialog(View view, Synchronization synchronization);
}
