package com.nichepharma.tcitapp.View.Callback;

import com.nichepharma.tcitapp.Repository.Model.Customer;


public interface IOnCustomerClicked {
    void onCustomerClick(Customer customer);
}
