package com.nichepharma.tcitapp.View.Adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.CallsItem;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.View.Activity.UserCustomersActivity;
import com.nichepharma.tcitapp.View.Callback.IOnDeleteSynchronization;
import com.nichepharma.tcitapp.View.Callback.IOnSyncClicked;
import com.nichepharma.tcitapp.View.Callback.IShowSyncDialog;
import com.nichepharma.tcitapp.View.Dialog.AddEditSynchronizationFeedbackDialog;
import com.nichepharma.tcitapp.databinding.ItemSynchronizationBinding;

import java.util.List;

public class SychronizationAdapter extends RecyclerView.Adapter<SychronizationAdapter.DoctorViewHolder> {

    private List<Synchronization> synchronizationList;
    private ItemSynchronizationBinding itemCustomerBinding;
    public IOnSyncClicked iOnSyncClicked;
    public IShowSyncDialog showSyncDialog;
    public IOnDeleteSynchronization onDeleteSynchronization;
    private Context context;

    public SychronizationAdapter(List<Synchronization> synchronizationList, IOnSyncClicked iOnSyncClicked,
                                 IShowSyncDialog showSyncDialog,
                                 IOnDeleteSynchronization onDeleteSynchronization,
                                 Context context) {
        this.synchronizationList = synchronizationList;
        this.iOnSyncClicked = iOnSyncClicked;
        this.showSyncDialog = showSyncDialog;
        this.onDeleteSynchronization = onDeleteSynchronization;
        this.context = context;
    }

    @NonNull
    @Override
    public DoctorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        DoctorViewHolder viewHolder;

        itemCustomerBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_synchronization, parent, false);
        viewHolder = new DoctorViewHolder(itemCustomerBinding, synchronizationList);


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull DoctorViewHolder holder, int position) {

        Synchronization synchronization = synchronizationList.get(position);
        int number = position + 1;
        itemCustomerBinding.sessionTextView.setText(
                String.valueOf(number) + ". Session: " + synchronization.getId());

        itemCustomerBinding.sessionCustomerTextView.setText(
                (synchronization.getCustomer() == null ? "" :
                        synchronization.getCustomer().getName() + " - "
                                + (synchronization.getCustomer().getSpeciality() == null ? "" : synchronization.getCustomer().getSpeciality())));

        String productName = "";
        for (int i = 0; i < synchronization.getCalls().size(); i++) {
            productName += synchronization.getCalls().get(i).getProduct().getName();
            if (i != synchronization.getCalls().size() - 1)
                productName += ", ";
        }
        itemCustomerBinding.productNameTextView.setText(productName);
        if (number % 2 == 0)
            itemCustomerBinding.sychronizationConstraintLayout.setBackground(itemCustomerBinding.getRoot().getResources().getDrawable(R.drawable.blue_row));
        else
            itemCustomerBinding.sychronizationConstraintLayout.setBackground(itemCustomerBinding.getRoot().getResources().getDrawable(R.drawable.white_row));


        if (isUserCustomerSet(synchronization))
            itemCustomerBinding.addCustomerTextView.setVisibility(View.GONE);
        else {
            itemCustomerBinding.addCustomerTextView.setVisibility(View.VISIBLE);
            itemCustomerBinding.syncImageView.setVisibility(View.GONE);
//            itemCustomerBinding.deleteSynchronizationTextView.setVisibility(View.GONE);
        }
        if (isAllProductHaveFeedBack(synchronization))
            itemCustomerBinding.addCommentImageView.setVisibility(View.GONE);
        else {
            itemCustomerBinding.addCommentImageView.setVisibility(View.VISIBLE);
            itemCustomerBinding.syncImageView.setVisibility(View.GONE);
//            itemCustomerBinding.deleteSynchronizationTextView.setVisibility(View.GONE);
        }
    }

    private boolean isUserCustomerSet(Synchronization synchronization) {
        return synchronization.getCustomer() != null;
    }

    private boolean isAllProductHaveFeedBack(Synchronization synchronization) {
        for (int i = 0; i < synchronization.getCalls().size(); i++) {
            CallsItem callsItem = synchronization.getCalls().get(i);
            if (callsItem.getCallObjection() == null || callsItem.getCallRemark() == null || callsItem.getSamples() <= 0)
                return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return synchronizationList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<Synchronization> removeItem(int position) {
        if (position != -1 ){
            synchronizationList.remove(position);
            notifyItemRemoved(position);
        }
        return synchronizationList;
    }

    class DoctorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemSynchronizationBinding itemProductBinding;
        List<Synchronization> synchronizationList;

        DoctorViewHolder(ItemSynchronizationBinding itemCustomerBinding,
                         final List<Synchronization> synchronizationList) {
            super(itemCustomerBinding.getRoot());
            this.itemProductBinding = itemCustomerBinding;
            this.synchronizationList = synchronizationList;

            itemProductBinding.syncImageView.setOnClickListener(this);
            itemProductBinding.addCommentImageView.setOnClickListener(this);
            itemProductBinding.addCustomerTextView.setOnClickListener(this);
            itemProductBinding.editImageView.setOnClickListener(this);
            itemCustomerBinding.deleteSynchronizationTextView.setOnClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showSyncDialog.showSyncDialog(view, synchronizationList.get(getAdapterPosition()));
                }
            });
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.sync_imageView:
                    itemProductBinding.syncImageView.setVisibility(View.GONE);
                    int position = getAdapterPosition();
                    iOnSyncClicked.syncClicked(synchronizationList.get(position), position);
                    break;
                case R.id.addComment_imageView:
                    new AddEditSynchronizationFeedbackDialog(context,
                            synchronizationList.get(getAdapterPosition()), Constants.ADD_FEEDBACK).show();
                    break;
                case R.id.addCustomer_textView:
                    Intent intent2 = new Intent(itemProductBinding.getRoot().getContext(), UserCustomersActivity.class);
                    intent2.putExtra(Constants.SYNCHRONIZATION_OBJ, synchronizationList.get(getAdapterPosition()));
                    itemProductBinding.getRoot().getContext().startActivity(intent2);
                    ((AppCompatActivity) itemProductBinding.getRoot().getContext()).finish();
                    break;
                case R.id.edit_imageView:

                    new AddEditSynchronizationFeedbackDialog(context,
                            synchronizationList.get(getAdapterPosition()), Constants.EDIT_FEEDBACK).show();
                    break;

                case R.id.deleteSynchronization_textView:
                    onDeleteSynchronization.onDeleteSynchronization(getAdapterPosition());
                    break;
            }
        }

    }
}
