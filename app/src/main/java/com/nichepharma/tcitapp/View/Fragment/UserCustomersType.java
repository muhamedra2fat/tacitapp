package com.nichepharma.tcitapp.View.Fragment;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Customer;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Repository.Model.UserCustomersResponse;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.Utils.FragmentUtils;
import com.nichepharma.tcitapp.View.Activity.MainActivity;
import com.nichepharma.tcitapp.View.Activity.UserCustomersActivity;
import com.nichepharma.tcitapp.ViewModel.CustomersViewModel;
import com.nichepharma.tcitapp.ViewModel.SychronizationViewModel;
import com.nichepharma.tcitapp.databinding.FragmentUserCustomersTypeBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class UserCustomersType extends Fragment implements View.OnClickListener {
    private FragmentUserCustomersTypeBinding fragmentUserCustomersTypeBinding;
    private CustomersViewModel customersViewModel;
    private HashMap<String, ArrayList<Customer>> map;
    private SychronizationViewModel sychronizationViewModel;

    private void init() {
        customersViewModel = ViewModelProviders.of(this).get(CustomersViewModel.class);
        sychronizationViewModel = ViewModelProviders.of(this).get(SychronizationViewModel.class);
    }

    private void setListeners() {
        fragmentUserCustomersTypeBinding.hospitalsTextView.setOnClickListener(this);
        fragmentUserCustomersTypeBinding.pharmacyTextView.setOnClickListener(this);
        fragmentUserCustomersTypeBinding.privateMarketsTextView.setOnClickListener(this);
        fragmentUserCustomersTypeBinding.newCallImageView.setOnClickListener(this);
        fragmentUserCustomersTypeBinding.newVisitImageView.setOnClickListener(this);
    }

    private Synchronization getSynchronization() {
        return (Synchronization) getArguments().getSerializable(Constants.SYNCHRONIZATION_OBJ);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentUserCustomersTypeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_customers_type, container, false);

        init();
        setListeners();
        getTypes();
        return fragmentUserCustomersTypeBinding.getRoot();
    }

    private void getTypes() {
        map = customersViewModel.getCustomersMap();
        if (map == null || (map.get(Constants.PRIVATE_MARKET).size() == 0 &&
                map.get(Constants.PHARMACY).size() == 0 &&map.get(Constants.HOSPITALS).size() == 0)) {
            observeViewModel();
        } else
            updateUi();
    }

    private void updateUi() {
        Animation animSlide = AnimationUtils.loadAnimation(getContext(),
                R.anim.slide_right);

        ArrayList<Customer> doctorsCustomers = map.get(Constants.PRIVATE_MARKET);
        if (doctorsCustomers.size() != 0) {
            fragmentUserCustomersTypeBinding.privateMarketsTextView.setVisibility(View.VISIBLE);
            fragmentUserCustomersTypeBinding.privateMarketsTextView.setAnimation(animSlide);
        }
        ArrayList<Customer> pharmacyCustomers = map.get(Constants.PHARMACY);
        if (pharmacyCustomers.size() != 0) {
            fragmentUserCustomersTypeBinding.pharmacyTextView.setVisibility(View.VISIBLE);
            fragmentUserCustomersTypeBinding.pharmacyTextView.setAnimation(animSlide);
        }
        ArrayList<Customer> hospitalsCustomers = map.get(Constants.HOSPITALS);
        if (hospitalsCustomers.size() != 0) {
            fragmentUserCustomersTypeBinding.hospitalsTextView.setVisibility(View.VISIBLE);
            fragmentUserCustomersTypeBinding.hospitalsTextView.setAnimation(animSlide);
        }

    }

    private void observeViewModel() {
        customersViewModel.getUserCustomers();
        customersViewModel.getUserCustomersLiveData().observe(this, new Observer<UserCustomersResponse>() {
            @Override
            public void onChanged(@Nullable UserCustomersResponse userCustomersResponse) {
                if (userCustomersResponse != null) {
                    if (userCustomersResponse.getStatus().equals("success")) {
                        map = getUsersTypes(userCustomersResponse.getResponse());
                        customersViewModel.addCustomersToSharedPreference(map);
                        updateUi();
                    }
                } else
                    Toast.makeText(getContext(), "Some thing went wrong", Toast.LENGTH_SHORT).
                            show();
            }
        });
    }

    private HashMap<String, ArrayList<Customer>> initCustomersHashMap() {
        HashMap<String, ArrayList<Customer>> map = new HashMap<>();
        map.put(Constants.PRIVATE_MARKET, new ArrayList<Customer>());
        map.put(Constants.PHARMACY, new ArrayList<Customer>());
        map.put(Constants.HOSPITALS, new ArrayList<Customer>());
        return map;
    }

    private HashMap<String, ArrayList<Customer>> getUsersTypes(List<Customer> customers) {
        HashMap<String, ArrayList<Customer>> map = initCustomersHashMap();
        if (customers != null) {
            for (int i = 0; i < customers.size(); i++) {
                Customer customer = customers.get(i);
                if (customer.getType().equals("Doctor")) {
                    ArrayList<Customer> doctorCustomers = map.get(Constants.PRIVATE_MARKET);
                    doctorCustomers.add(customer);
                    map.put(Constants.PRIVATE_MARKET, doctorCustomers);
                } else if (customer.getType().equals("Hospital")) {
                    ArrayList<Customer> hospitalsCustomers = map.get(Constants.HOSPITALS);
                    hospitalsCustomers.add(customer);
                    map.put(Constants.HOSPITALS, hospitalsCustomers);
                } else if (customer.getType().equals("Pharmacy")) {
                    ArrayList<Customer> pharmacyCustomers = map.get(Constants.PHARMACY);
                    pharmacyCustomers.add(customer);
                    map.put(Constants.PHARMACY, pharmacyCustomers);
                }
            }


        }
        return map;

    }

    private void startUserCustomerFragment(ArrayList<Customer> customers , boolean isHospital) {
        sychronizationViewModel.saveSynchronizationSession();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.CUSTOMERS, customers);
        bundle.putBoolean(Constants.IS_HOSPITAL,isHospital);
        bundle.putSerializable(Constants.SYNCHRONIZATION_OBJ, getSynchronization());
        UserCustomersFragment userCustomersFragment = new UserCustomersFragment();
        userCustomersFragment.setArguments(bundle);
        FragmentUtils.startFragment(getContext(), userCustomersFragment, R.id.userCustomersFragment_constraintLayout, true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.hospitals_textView:
                startUserCustomerFragment(map.get(Constants.HOSPITALS),true);
                break;
            case R.id.privateMarkets_textView:
                startUserCustomerFragment(map.get(Constants.PRIVATE_MARKET),false);

                break;
            case R.id.pharmacy_textView:
                startUserCustomerFragment(map.get(Constants.PHARMACY),false);
                break;
            case R.id.newCall_imageView:
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                break;
            case R.id.newVisit_imageView:
                sychronizationViewModel.saveSynchronizationSession();
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                break;
        }
    }
}
