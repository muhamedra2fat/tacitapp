package com.nichepharma.tcitapp.View.Adapter;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Product;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.Utils.FilesUtils;
import com.nichepharma.tcitapp.View.Activity.PresentationActivity;
import com.nichepharma.tcitapp.ViewModel.ProductViewModel;
import com.nichepharma.tcitapp.databinding.ItemProductBinding;

import java.io.File;
import java.util.List;

import okhttp3.ResponseBody;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> productsList;
    private ItemProductBinding itemProductBinding;

    public ProductAdapter(List<Product> productsResponseList) {
        this.productsList = productsResponseList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;

        itemProductBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_product, parent, false);
        viewHolder = new ProductViewHolder(itemProductBinding, productsList);


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {


        ProductViewHolder productViewHolder = (ProductViewHolder) holder;
        Product product = productsList.get(position);

        Glide.with(productViewHolder.itemProductBinding.getRoot())
                .load(Constants.APP_URL+ "appIcon/" + product.getPath_name().replaceAll(".zip",".png"))
                .into(productViewHolder.itemProductBinding.productImgImageView);


        if (isDownloaded(product.getPath_name()))
            productViewHolder.itemProductBinding.productImgImageView.setAlpha(1f);
        else
            productViewHolder.itemProductBinding.productImgImageView.setAlpha(0.5f);
    }

    private boolean isDownloaded(String pathName) {
        File file = new File(itemProductBinding.getRoot().getContext().getExternalFilesDir(null) + File.separator + pathName.replace(".zip", ""));
        return file.isDirectory();
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemProductBinding itemProductBinding;
        List<Product> productsList;
        Context context;

        ProductViewHolder(ItemProductBinding itemProductBinding, List<Product> productsList) {
            super(itemProductBinding.getRoot());
            this.itemProductBinding = itemProductBinding;
            itemProductBinding.productImgImageView.setOnClickListener(this);
            this.productsList = productsList;
            this.context = itemProductBinding.getRoot().getContext();
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.productImg_imageView:
                    int position = getAdapterPosition();
                    if (isDownloaded(productsList.get(position).getPath_name()))
                        startPresentationActivity(productsList.get(position));
                    else {
                        itemProductBinding.progressBar.setVisibility(View.VISIBLE);
                        observeDownloadFile(productsList.get(position));
                    }
                    break;
            }
        }

        private void startPresentationActivity(Product product) {
            Intent intent = new Intent(context, PresentationActivity.class);
            intent.putExtra(Constants.PRODUCT_OBJ, product);
            context.startActivity(intent);
            ((AppCompatActivity) context).finish();
        }


        private void observeDownloadFile(final Product product) {
            final String zipName = product.getPath_name();
            final String path = Constants.PRODUCT_URL + zipName;
            ProductViewModel productViewModel = new ProductViewModel(((AppCompatActivity) context).getApplication());
            productViewModel.downloadFile(path);
            productViewModel.getDownloadFileLiveData().observe((AppCompatActivity) context, new Observer<ResponseBody>() {
                @Override
                public void onChanged(@Nullable ResponseBody responseBody) {
                    itemProductBinding.progressBar.setVisibility(View.GONE);
                    if (responseBody != null) {
                        if (FilesUtils.writeResponseBodyToDisk(context,responseBody, zipName)) {
                            if (FilesUtils.unzipFile(context.getExternalFilesDir(null) + File.separator + zipName.replace(".zip", "") + File.separator + zipName, zipName)) {
                                startPresentationActivity(product);
                            }

                        }

                    } else
                        Toast.makeText(context, "SomeThing went wrong", Toast.LENGTH_SHORT).show();
                }
            });
        }




    }
}
