package com.nichepharma.tcitapp.View.Fragment;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Customer;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.Utils.RecyclerViewUtils;
import com.nichepharma.tcitapp.View.Activity.FeedbackActivity;
import com.nichepharma.tcitapp.View.Activity.MainActivity;
import com.nichepharma.tcitapp.View.Activity.SychronizationActivity;
import com.nichepharma.tcitapp.View.Adapter.DoctorAdapter;
import com.nichepharma.tcitapp.View.Adapter.UserCustomersAdapter;
import com.nichepharma.tcitapp.View.Callback.IOnCustomerClicked;
import com.nichepharma.tcitapp.View.Callback.IOnHospitalClicked;
import com.nichepharma.tcitapp.ViewModel.CustomersViewModel;
import com.nichepharma.tcitapp.ViewModel.SychronizationViewModel;
import com.nichepharma.tcitapp.databinding.FragmentUserCustomersBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class UserCustomersFragment extends Fragment implements
        IOnCustomerClicked, View.OnClickListener, TextWatcher, IOnHospitalClicked {
    private SychronizationViewModel sychronizationViewModel;
    private FragmentUserCustomersBinding fragmentUserCustomersBinding;
    private UserCustomersAdapter userCustomersAdapter;
    private Customer customerClicked;
    private ArrayList<Customer> customers, doctors = new ArrayList<>();
    private boolean isHospitalType;

    private void init() {
        sychronizationViewModel = ViewModelProviders.of(this).get(SychronizationViewModel.class);
        getCustomerType();
    }

    private void setListeners() {
        fragmentUserCustomersBinding.doneImageView.setOnClickListener(this);
        fragmentUserCustomersBinding.homeImageView.setOnClickListener(this);
        fragmentUserCustomersBinding.backImageView.setOnClickListener(this);
        fragmentUserCustomersBinding.cancelSearchImageView.setOnClickListener(this);
        fragmentUserCustomersBinding.searchEditText.addTextChangedListener(this);
    }

    private Synchronization getSynchronization() {
        return (Synchronization) getArguments().getSerializable(Constants.SYNCHRONIZATION_OBJ);
    }

    private void getCustomerType() {
        isHospitalType = getArguments().getBoolean(Constants.IS_HOSPITAL);
    }

    private ArrayList<Customer> getCustomersList() {
        if (customers == null)
            customers = getArguments().getParcelableArrayList(Constants.CUSTOMERS);
        return customers;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentUserCustomersBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_customers, container, false);
        init();
        setListeners();
        getCustomers();
        return fragmentUserCustomersBinding.getRoot();
    }


    private void getCustomers() {
        ArrayList<Customer> customers = getCustomersList();
        userCustomersAdapter = new UserCustomersAdapter(customers, this);
        RecyclerViewUtils.setupRecyclerView(getContext(), userCustomersAdapter,
                fragmentUserCustomersBinding.customersRecyclerView,
                true);


    }

    @Override
    public void onCustomerClick(Customer customer) {
        customerClicked = customer;
        doctors.clear();
        if (customer == null) {
            RecyclerViewUtils.setupRecyclerView(getContext(),
                    new DoctorAdapter(new ArrayList<Customer>(), this),
                    fragmentUserCustomersBinding.doctorRecyclerView, true);
        } else {
            List<Customer> customerList = new ArrayList<>();
            if (customer.getDoctors() == null || customer.getDoctors().size() == 0) {
                customerList.add(customer);
            } else
                customerList = customer.getDoctors();

            RecyclerViewUtils.setupRecyclerView(getContext(),
                    new DoctorAdapter(customerList, isHospitalType ? this : null),
                    fragmentUserCustomersBinding.doctorRecyclerView, true);
        }
    }

    @Override
    public void onDoctorClick(Customer customer) {
        if (isHospitalType) {
            if (doctors.contains(customer)) {
                doctors.remove(customer);
            } else {
                doctors.add(customer);
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.done_imageView:
                if (customerClicked == null || (isHospitalType && doctors.size() == 0))
                    Toast.makeText(getContext(), "Please choose customer", Toast.LENGTH_SHORT).show();

                else {
                    Synchronization synchronization = sychronizationViewModel.setSychronizationCustomerAndLocation(
                            customerClicked, getSynchronization(), doctors);
                    sychronizationViewModel.updateSynchronization(synchronization);
                    Intent intent = new Intent(getActivity(), SychronizationActivity.class);
                    startActivity(intent);
                    (getActivity()).finish();
                }
                break;
            case R.id.home_imageView:
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                break;

            case R.id.back_imageView:
                getActivity().onBackPressed();
                break;
            case R.id.cancelSearch_imageView:
                fragmentUserCustomersBinding.searchEditText.setText("");
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        onCustomerClick(null);
        if (s.length() >= 1) {
            ArrayList<Customer> customerArrayList = getCustomersList();
            ArrayList<Customer> searchedCustomerList = new ArrayList<>();
            for (int i = 0; i < customerArrayList.size(); i++) {
                Customer temp = customerArrayList.get(i);
                if (temp.getName().toLowerCase().contains(s.toString().toLowerCase()))
                    searchedCustomerList.add(temp);
            }
            userCustomersAdapter = new UserCustomersAdapter(searchedCustomerList, this);
            RecyclerViewUtils.setupRecyclerView(getContext(), userCustomersAdapter,
                    fragmentUserCustomersBinding.customersRecyclerView,
                    true);
        } else if (s.length() == 0)
            getCustomers();

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


}
