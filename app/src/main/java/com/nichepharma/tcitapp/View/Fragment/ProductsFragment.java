package com.nichepharma.tcitapp.View.Fragment;


import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Product;
import com.nichepharma.tcitapp.Repository.Model.ProductsResponse;
import com.nichepharma.tcitapp.Utils.FragmentUtils;
import com.nichepharma.tcitapp.Utils.NetworkUtils;
import com.nichepharma.tcitapp.Utils.RecyclerViewUtils;
import com.nichepharma.tcitapp.View.Activity.LoginActivity;
import com.nichepharma.tcitapp.View.Activity.SychronizationActivity;
import com.nichepharma.tcitapp.View.Adapter.ProductAdapter;
import com.nichepharma.tcitapp.ViewModel.CustomersViewModel;
import com.nichepharma.tcitapp.ViewModel.LoginViewModel;
import com.nichepharma.tcitapp.ViewModel.ProductViewModel;
import com.nichepharma.tcitapp.ViewModel.SychronizationViewModel;
import com.nichepharma.tcitapp.databinding.FragmentProductsBinding;

import java.util.List;


public class ProductsFragment extends Fragment implements View.OnClickListener {
    private ProductViewModel productViewModel;
    private FragmentProductsBinding fragmentProductsBinding;

    private void init() {
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);

    }

    private void setListeners() {
        fragmentProductsBinding.reloginImageView.setOnClickListener(this);
        fragmentProductsBinding.syncImageView.setOnClickListener(this);
        fragmentProductsBinding.updateVersionLinearLayout.setOnClickListener(this);
    }

    private boolean checkProducts() {
        List<Product> products = productViewModel.getProductsFromSharedPrefernce();
        if (products == null || products.size() == 0)
            return false;

        RecyclerViewUtils.setupRecyclerView(getContext(), new ProductAdapter(products),
                fragmentProductsBinding.productsRecyclerView, false);
        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentProductsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_products, container, false);
        View view = fragmentProductsBinding.getRoot();
        init();
        setListeners();
        if (!checkProducts())
            observeViewModel();

        return view;
    }

    private void observeViewModel() {
        productViewModel.getProducts();
        productViewModel.getProductsLiveData().observe(this, new Observer<ProductsResponse>() {
            @Override
            public void onChanged(@Nullable ProductsResponse productsResponse) {
                if (productsResponse != null)
                    if (productsResponse.getStatus().equals("success")) {
                        productViewModel.saveProductsIntoSharedPreference(productsResponse.getResponse());
                        RecyclerViewUtils.setupRecyclerView(getContext(), new ProductAdapter(productsResponse.getResponse()),
                                fragmentProductsBinding.productsRecyclerView, false);


                    }
            }
        });
    }

    private void showLogOutAlertDialog() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        builder.setTitle("Message")
                .setMessage("Are you sure you want to logout , all your unfinished session  will be deleted ?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        LoginViewModel loginViewModel = ViewModelProviders.of(ProductsFragment.this).get(LoginViewModel.class);
                        SychronizationViewModel sychronizationViewModel = ViewModelProviders.of(ProductsFragment.this).get(SychronizationViewModel.class);
                        CustomersViewModel customersViewModel = ViewModelProviders.of(ProductsFragment.this).get(CustomersViewModel.class);

                        loginViewModel.logout();
                        productViewModel.deleteProducts();
                        sychronizationViewModel.deleteVisitsAndCalls();
                        customersViewModel.deleteCustomers();

                        startActivity(new Intent(getActivity(), LoginActivity.class));
                        getActivity().finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.relogin_imageView:
                showLogOutAlertDialog();
                break;
            case R.id.sync_imageView:
                startActivity(new Intent(getActivity(), SychronizationActivity.class));
                break;
            case R.id.updateVersion_linearLayout:
                if (NetworkUtils.isNetworkAvailable(getContext())) {
                    FragmentUtils.startFragment(getContext(), new ProductVersionFragment(),
                            R.id.fragment_container, true);
                } else {
                    Toast.makeText(getContext(), "Please Check your internet connection", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
}
