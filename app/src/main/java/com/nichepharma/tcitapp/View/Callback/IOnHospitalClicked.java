package com.nichepharma.tcitapp.View.Callback;

import com.nichepharma.tcitapp.Repository.Model.Customer;

public interface IOnHospitalClicked {
    void onDoctorClick(Customer customer);

}
