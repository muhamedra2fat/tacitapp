package com.nichepharma.tcitapp.View.Adapter;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Product;
import com.nichepharma.tcitapp.Repository.Model.Versions.Version;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.View.Activity.FeedbackActivity;
import com.nichepharma.tcitapp.View.Callback.IVersionHelper;
import com.nichepharma.tcitapp.ViewModel.ProductViewModel;
import com.nichepharma.tcitapp.databinding.ItemProductVersionBinding;

import java.util.List;

public class ProductVersionAdapter extends RecyclerView.Adapter<ProductVersionAdapter.AddEditSingleProductFeedbackViewHolder> {

    private List<Version> versionsList;
    private ItemProductVersionBinding itemProductVersionBinding;
    private IVersionHelper iVersionHelper;

    public ProductVersionAdapter(IVersionHelper iVersionHelper, List<Version> versionsList) {
        this.versionsList = versionsList;
        this.iVersionHelper = iVersionHelper;
    }

    public void setVersionsList(List<Version> versionsList) {
        this.versionsList.clear();
        this.versionsList.addAll(versionsList);
        notifyDataSetChanged();
    }

    public void setLoadingIcon(String productId, int status) {
        for (int i = 0; i < versionsList.size(); i++) {
            if (versionsList.get(i).getId().equals(productId)) {
                versionsList.get(i).setLoading(status);
                notifyItemChanged(i);
                return;
            }
        }
    }


    @NonNull
    @Override
    public AddEditSingleProductFeedbackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        AddEditSingleProductFeedbackViewHolder viewHolder;

        itemProductVersionBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_product_version, parent, false);
        viewHolder = new AddEditSingleProductFeedbackViewHolder(itemProductVersionBinding);


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull AddEditSingleProductFeedbackViewHolder holder, int position) {
        Version version = versionsList.get(position);

        holder.itemProductVersionBinding.productNameTextView.setText(version.getName());
        holder.itemProductVersionBinding.availableVersionTextView.setText(String.valueOf(version.getVersion()));

        double currentVersion = iVersionHelper.getCurrentVersion(version.getId());


        if (currentVersion == version.getVersion())
            version.setLoading(Product.DOWNLOADED_STATUS);
        else {
            // do nothing
        }

        if (version.isLoading() == Product.NOT_DOWNLOADED_STATUS) {
            holder.itemProductVersionBinding.updateVersionTextView.setTextColor(Color.parseColor("#0000FF"));
            holder.itemProductVersionBinding.updateVersionTextView.setEnabled(true);
        } else {
            holder.itemProductVersionBinding.updateVersionTextView.setTextColor(Color.parseColor("#555555"));
            holder.itemProductVersionBinding.updateVersionTextView.setEnabled(false);
        }
        switch (version.isLoading()) {
            case Product.DOWNLOADED_STATUS:
                holder.itemProductVersionBinding.updateVersionTextView.setText("Updated");
                holder.itemProductVersionBinding.loadingProgressBar.setVisibility(View.GONE);
                holder.itemProductVersionBinding.updateVersionTextView.setVisibility(View.VISIBLE);

                break;
            case Product.NOT_DOWNLOADED_STATUS:
                holder.itemProductVersionBinding.updateVersionTextView.setText("Update");
                holder.itemProductVersionBinding.loadingProgressBar.setVisibility(View.GONE);
                holder.itemProductVersionBinding.updateVersionTextView.setVisibility(View.VISIBLE);
                break;
            case Product.LOADING_STATUS:
                holder.itemProductVersionBinding.loadingProgressBar.setVisibility(View.VISIBLE);
                holder.itemProductVersionBinding.updateVersionTextView.setVisibility(View.GONE);
                break;
            case Product.ERROR_STATUS:
                holder.itemProductVersionBinding.updateVersionTextView.setText("Not Downloadable");
                holder.itemProductVersionBinding.loadingProgressBar.setVisibility(View.GONE);
                holder.itemProductVersionBinding.updateVersionTextView.setVisibility(View.VISIBLE);
                break;
        }

        if (currentVersion > 0.0) {
            holder.itemProductVersionBinding.currentVersionTextView.setText(String.valueOf(currentVersion));
        } else
            holder.itemProductVersionBinding.currentVersionTextView.setText("_");


    }

    @Override
    public int getItemCount() {
        return versionsList.size();
    }


    class AddEditSingleProductFeedbackViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemProductVersionBinding itemProductVersionBinding;

        AddEditSingleProductFeedbackViewHolder(ItemProductVersionBinding itemProductVersionBinding) {
            super(itemProductVersionBinding.getRoot());
            this.itemProductVersionBinding = itemProductVersionBinding;
            itemProductVersionBinding.updateVersionTextView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.updateVersion_textView:
                    int position = getAdapterPosition();
                    itemProductVersionBinding.loadingProgressBar.setVisibility(View.VISIBLE);
                    itemProductVersionBinding.updateVersionTextView.setVisibility(View.GONE);
                    iVersionHelper.updateProduct(versionsList.get(position).getId(), versionsList.get(position).getVersion());
                    break;
            }
        }


    }
}
