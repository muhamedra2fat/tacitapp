package com.nichepharma.tcitapp.View.Activity;


import android.support.v4.app.FragmentManager;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.github.barteksc.pdfviewer.util.FileUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Bookmark;
import com.nichepharma.tcitapp.Repository.Model.Menu.Menu;
import com.nichepharma.tcitapp.Repository.Model.Menu.MenuItem;
import com.nichepharma.tcitapp.Repository.Model.PDF.Content;
import com.nichepharma.tcitapp.Repository.Model.PDF.PDFResponse;
import com.nichepharma.tcitapp.Repository.Model.Product;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.Utils.CustomViewPager;
import com.nichepharma.tcitapp.Utils.DateUtils;
import com.nichepharma.tcitapp.Utils.FilesUtils;
import com.nichepharma.tcitapp.Utils.RecyclerViewUtils;
import com.nichepharma.tcitapp.View.Adapter.MenuAdapter;
import com.nichepharma.tcitapp.View.Adapter.ReferenceAdapter;
import com.nichepharma.tcitapp.View.Adapter.ScreenSlidePagerAdapter;
import com.nichepharma.tcitapp.View.Callback.IOnMenuItemClicked;
import com.nichepharma.tcitapp.ViewModel.SychronizationViewModel;
import com.nichepharma.tcitapp.databinding.ActivityPresentationBinding;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PresentationActivity extends FragmentActivity implements View.OnClickListener, IOnMenuItemClicked {

    private HashMap<Integer, Long> durationHashmap = new HashMap<>();
    private int currentPosition = 0;
    private long start = 0, end = 0;
    private String timeStamp;
    private HashMap<String, PDFResponse> referenceMap;
    private ActivityPresentationBinding activityPresentationBinding;
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private CustomViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    private Bookmark bookmark;
    private SychronizationViewModel sychronizationViewModel;

    private void initHashMap(int size) {
        for (int i = 0; i < size; i++) {
            long zero = 0;
            durationHashmap.put(i, zero);
        }
    }

    private void initDrawer() {
        Menu menu = readMenuJsonFromFile();
        if (!menu.getMenuButton().isEmpty()) {
            activityPresentationBinding.menuIconImageView.setImageDrawable(
                    Drawable.createFromPath(getExternalFilesDir(null) + File.separator + getProductName() + File.separator + menu.getMenuButton())
            );
        }
        activityPresentationBinding.menuRecyclerView.setBackground(
                Drawable.createFromPath(getExternalFilesDir(null) + File.separator + getProductName() + File.separator + menu.getMenuBKG())
        );

        if (menu.getContent().size() > 0)
            activityPresentationBinding.menuRecyclerView.setPadding(
                    0, Integer.parseInt(menu.getContent().get(0).getMenuY()), 0, 0);


        RecyclerViewUtils.setupRecyclerView(this,
                new MenuAdapter(menu.getContent(), this, getProductName()),
                activityPresentationBinding.menuRecyclerView,
                true

        );
    }

    public Bookmark readBookMarkJsonFromFile() {

        String path = getExternalFilesDir(null) + File.separator + getProductName() + File.separator + "bookmark.json";
        final Type REVIEW_TYPE = new TypeToken<Bookmark>() {
        }.getType();
        Gson gson = new Gson();
        JsonReader reader = null;
        try {
//            reader = new JsonReader(new FileReader(path));
            String content = FilesUtils.getStringFromFile(path);
            content = content.replaceAll("\\,(?=\\s*?[\\}\\]])", "");
            Bookmark data = gson.fromJson(content, REVIEW_TYPE); // contains the whole reviews list
            Log.d("Presentation", "Bookmark read json: " + data.toString());
            return data;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("Presentation", "Bookmark read json error: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Menu readMenuJsonFromFile() {

        String path = getExternalFilesDir(null) + File.separator + getProductName() + File.separator + "menu.json";
        final Type REVIEW_TYPE = new TypeToken<Menu>() {
        }.getType();
        Gson gson = new Gson();
        JsonReader reader = null;
        try {
//            reader = new JsonReader(new FileReader(path));
            String content = FilesUtils.getStringFromFile(path);
            content = content.replaceAll("\\,(?=\\s*?[\\}\\]])", "");
            Menu data = gson.fromJson(content, REVIEW_TYPE); // contains the whole reviews list
            Log.d("Presentation", "read json: " + data.toString());
            return data;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("Presentation", "read json error: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public HashMap<String, PDFResponse> readPDFJsonFromFile() {

        String path = getExternalFilesDir(null) + File.separator + getProductName() + File.separator + "pdf.json";
        final Type REVIEW_TYPE = new TypeToken<HashMap<String, PDFResponse>>() {
        }.getType();
        Gson gson = new Gson();
        JsonReader reader = null;
        try {

//            reader = new JsonReader(new FileReader(path));
            String content = FilesUtils.getStringFromFile(path);
            content = content.replaceAll("\\,(?=\\s*?[\\}\\]])", "");
            HashMap<String, PDFResponse> data = gson.fromJson(content, REVIEW_TYPE); // contains the whole reviews list
            Log.d("Presentation", "read pdf json: " + data.toString());
            return data;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.d("Presentation", "read pdf json error: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    private void initRefrences() {
        referenceMap = readPDFJsonFromFile();
        activityPresentationBinding.recyclerView.setVisibility(View.GONE);
    }

    private void initPager() {
        mPager = findViewById(R.id.pager);
        bookmark = readBookMarkJsonFromFile();
        if (bookmark != null && bookmark.getSlideshows() != null && bookmark.getSlideshows().size() > 0) {
            // Instantiate a ViewPager and a PagerAdapter.

            mPager.setOffscreenPageLimit(0);
            mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), bookmark.getSlideshows(), getProductName());
            initHashMap(bookmark.getSlideshows().size());

            start = System.currentTimeMillis();
            mPager.setAdapter(mPagerAdapter);
            mPager.setOffscreenPageLimit(0);

            String companyLogo = getExternalFilesDir(null) + File.separator + getProductName()
                    + File.separator + bookmark.getCompanyLogo().get(0).getImgPath();
            Drawable d = Drawable.createFromPath(companyLogo);

            activityPresentationBinding.companyLogoImageView.setImageDrawable(d);
        }
    }


    private String getProductName() {
        return getProduct().getPath_name().replace(".zip", "");
    }

    private Product getProduct() {
        return (Product) getIntent().getExtras().getSerializable(Constants.PRODUCT_OBJ);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityPresentationBinding = DataBindingUtil.setContentView(this, R.layout.activity_presentation);

        initPager();
        initDrawer();
        initRefrences();
        setListeners();
        activityPresentationBinding.doneImageView.setVisibility(View.GONE);
        activityPresentationBinding.companyLogoImageView.setVisibility(View.GONE);
        activityPresentationBinding.openCloseReferenceListImageView.setVisibility(View.GONE);

        sychronizationViewModel = ViewModelProviders.of(this).get(SychronizationViewModel.class);
        timeStamp = DateUtils.getProductTimeStamp();

//        ReferenceResponse referenceResponse = readPDFJsonFromFile();
//        if (referenceResponse != null)
//            RecyclerViewUtils.setupRecyclerView(this, new ReferenceAdapter(referenceResponse.getReferenceList()), activityPresentationBinding.recyclerView, true);

    }

    private void calculateDuration(boolean isNext) {

        end = System.currentTimeMillis();
        long duration = (end - start) / 1000;
        start = end;
        int pos;
        if (isNext)
            pos = currentPosition - 1;
        else
            pos = currentPosition + 1;
        long positionDuration = durationHashmap.get(pos);
        durationHashmap.put(pos, positionDuration + duration);

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() == 0) {
            startActivity(new Intent(PresentationActivity.this, MainActivity.class));
            this.finish();
        } else
            super.onBackPressed();
    }

    private void setListeners() {
        activityPresentationBinding.openCloseReferenceListImageView.setOnClickListener(this);
        activityPresentationBinding.menuIconImageView.setOnClickListener(this);
        activityPresentationBinding.companyLogoImageView.setOnClickListener(this);
        mPager.setOnPageChangeListener(new CustomViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                activityPresentationBinding.doneImageView.setVisibility(View.GONE);
                activityPresentationBinding.recyclerView.setVisibility(View.GONE);

                if (referenceMap.containsKey(String.valueOf(position))) {
                    List<Content> contentList = referenceMap.get(String.valueOf(position)).getContent();
                    if (contentList != null) {
                        RecyclerViewUtils.setupRecyclerView(PresentationActivity.this,
                                new ReferenceAdapter(contentList, getProductName()),
                                activityPresentationBinding.recyclerView,
                                true);

                    }
                    activityPresentationBinding.openCloseReferenceListImageView.setVisibility(View.VISIBLE);
                } else {
                    activityPresentationBinding.openCloseReferenceListImageView.setVisibility(View.GONE);
                    RecyclerViewUtils.setupRecyclerView(PresentationActivity.this,
                            new ReferenceAdapter(new ArrayList<Content>(), getProductName()),
                            activityPresentationBinding.recyclerView,
                            true);
                }

                if (bookmark != null && (position == 0 || position == bookmark.getSlideshows().size() - 1)) {
                    activityPresentationBinding.companyLogoImageView.setVisibility(View.GONE);
                    if (position == bookmark.getSlideshows().size() - 1) {
                        activityPresentationBinding.doneImageView.setVisibility(View.VISIBLE);
                    }
                } else {
                    activityPresentationBinding.companyLogoImageView.setVisibility(View.VISIBLE);
                }

                boolean isNext = true;
                if (currentPosition > position)
                    isNext = false;
                currentPosition = position;

                calculateDuration(isNext);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        activityPresentationBinding.doneImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transferToNextActivity();
                PresentationActivity.this.finish();
            }
        });

    }

    private void transferToNextActivity() {
        Synchronization synchronization = sychronizationViewModel.addProduct(getProduct(), durationHashmap, timeStamp);
        currentPosition++;
        calculateDuration(true);

        Intent intent = new Intent(PresentationActivity.this, UserCustomersActivity.class);
        intent.putExtra(Constants.SLIDES_MAP, durationHashmap);
        intent.putExtra(Constants.SYNCHRONIZATION_OBJ, synchronization);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.openCloseReferenceList_imageView:
                android.view.animation.Animation animSlide;
                if (activityPresentationBinding.recyclerView.getVisibility() == View.GONE) {
                    activityPresentationBinding.recyclerView.setVisibility(View.VISIBLE);
                    animSlide = AnimationUtils.loadAnimation(this,
                            android.support.v7.appcompat.R.anim.abc_slide_in_bottom);
                    activityPresentationBinding.openCloseReferenceListImageView.setAnimation(animSlide);
                } else {
                    activityPresentationBinding.recyclerView.setVisibility(View.GONE);
                    animSlide = AnimationUtils.loadAnimation(this,
                            android.support.v7.appcompat.R.anim.abc_slide_out_bottom);
                }
                animSlide.setDuration(200);
                activityPresentationBinding.recyclerView.setAnimation(animSlide);

                break;
            case R.id.menuIcon_imageView:

                break;
            case R.id.companyLogo_imageView:
                mPager.setCurrentItem(bookmark.getSlideshows().size() - 1, true);
                break;
        }
    }

    @Override
    public void menuItemClicked(MenuItem menuItem) {
        activityPresentationBinding.drawerLayout.closeDrawer(Gravity.START);
        mPager.setCurrentItem(Integer.parseInt(menuItem.getGoTo()));
    }
}
