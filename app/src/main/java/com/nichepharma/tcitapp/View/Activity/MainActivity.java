package com.nichepharma.tcitapp.View.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Utils.FragmentUtils;
import com.nichepharma.tcitapp.View.Fragment.ProductsFragment;
import com.nichepharma.tcitapp.ViewModel.SychronizationViewModel;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentUtils.startFragment(this, new ProductsFragment(),
                R.id.fragment_container, false);


    }
}
