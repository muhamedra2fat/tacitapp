package com.nichepharma.tcitapp.View.Callback;

import com.nichepharma.tcitapp.Repository.Model.Menu.MenuItem;

public interface IOnMenuItemClicked {
    void menuItemClicked(MenuItem menuItem);
}
