package com.nichepharma.tcitapp.View.Adapter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.PDF.Content;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.Utils.FragmentUtils;
import com.nichepharma.tcitapp.View.Activity.FeedbackActivity;
import com.nichepharma.tcitapp.View.Activity.UserCustomersActivity;
import com.nichepharma.tcitapp.View.Fragment.PDFFragment;
import com.nichepharma.tcitapp.databinding.ItemReferenceBinding;

import java.io.File;
import java.util.List;

public class ReferenceAdapter extends RecyclerView.Adapter<ReferenceAdapter.ReferenceViewHolder> {
    private List<Content> referenceList;
    private ItemReferenceBinding itemReferenceBinding;
private String productName;
    public ReferenceAdapter(List<Content> referenceList , String productName) {
        this.referenceList = referenceList;
        this.productName = productName;

    }

    @NonNull
    @Override
    public ReferenceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        ReferenceViewHolder viewHolder;

        itemReferenceBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_reference, parent, false);
        viewHolder = new ReferenceViewHolder(itemReferenceBinding);


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull ReferenceViewHolder holder, int position) {
        if (referenceList.get(position) != null) {
            File imgFile = new File(holder.itemReferenceBinding.getRoot().getContext().getExternalFilesDir(null) +
                    File.separator + productName + File.separator + referenceList.get(position).getImage());
            if (imgFile.exists())
                holder.itemReferenceBinding.refNameImageView.setImageURI(Uri.fromFile(imgFile));


        }
    }


    @Override
    public int getItemCount() {
        return referenceList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class ReferenceViewHolder extends RecyclerView.ViewHolder {
        private ItemReferenceBinding itemReferenceBinding;

        ReferenceViewHolder(ItemReferenceBinding itemReferenceBinding) {
            super(itemReferenceBinding.getRoot());
            this.itemReferenceBinding = itemReferenceBinding;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.PRODUCT_NAME, productName);
                    bundle.putString(Constants.REFERENCE_PATH, referenceList.get(getAdapterPosition()).getRef());
                    PDFFragment pdfFragment = new PDFFragment();
                    pdfFragment.setArguments(bundle);
                    FragmentUtils.startFragment(itemView.getContext(),
                            pdfFragment,
                            R.id.presentationContainer_constraintLayout,
                            true);
                }
            });
        }


    }
}
