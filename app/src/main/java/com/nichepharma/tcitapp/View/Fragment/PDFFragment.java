package com.nichepharma.tcitapp.View.Fragment;

import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.databinding.FragmentPdfBinding;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.util.List;

public class PDFFragment extends Fragment implements OnPageChangeListener, OnLoadCompleteListener {
    private static final String TAG = PDFFragment.class.getSimpleName();
    public String SAMPLE_FILE;
    Integer pageNumber = 0;
    String pdfFileName;
    private FragmentPdfBinding fragmentPDFBinding;

    private String getProductName() {
        return getArguments().getString(Constants.PRODUCT_NAME);
    }

    private String getReferncePath() {
        return getArguments().getString(Constants.REFERENCE_PATH);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentPDFBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_pdf, container, false);
        View view = fragmentPDFBinding.getRoot();

        displayFromAsset();
        return view;


    }

    private void displayFromAsset() {

        SAMPLE_FILE = getActivity().getExternalFilesDir(null) + File.separator + getProductName() +
                File.separator + getReferncePath();
        if (!SAMPLE_FILE.contains(".pdf"))
            SAMPLE_FILE += ".pdf";
        fragmentPDFBinding.pdfView.fromFile(new File(SAMPLE_FILE))
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(getContext()))
                .load();
    }


    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        getActivity().setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }


    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = fragmentPDFBinding.pdfView.getDocumentMeta();
        printBookmarksTree(fragmentPDFBinding.pdfView.getTableOfContents(), "-");

    }


    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

}
