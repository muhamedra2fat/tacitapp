package com.nichepharma.tcitapp.View.Activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.Utils.FragmentUtils;
import com.nichepharma.tcitapp.View.Fragment.UserCustomersFragment;
import com.nichepharma.tcitapp.View.Fragment.UserCustomersType;
import com.nichepharma.tcitapp.databinding.ActivityDoctorsBinding;

import java.util.HashMap;


public class UserCustomersActivity extends AppCompatActivity {
    private ActivityDoctorsBinding activityDoctorsBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityDoctorsBinding = DataBindingUtil.setContentView(this, R.layout.activity_doctors);
        HashMap<Integer, Long> duration = (HashMap<Integer, Long>) getIntent().getExtras().get(Constants.SLIDES_MAP);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.SLIDES_MAP, duration);
        bundle.putSerializable(Constants.SYNCHRONIZATION_OBJ, getIntent().getExtras().getSerializable(Constants.SYNCHRONIZATION_OBJ));
        UserCustomersType userCustomersFragment = new UserCustomersType();
        userCustomersFragment.setArguments(bundle);
        FragmentUtils.startFragment(this,
                userCustomersFragment,
                R.id.userCustomersFragment_constraintLayout,
                false);
    }


}
