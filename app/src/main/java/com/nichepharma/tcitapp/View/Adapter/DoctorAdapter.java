package com.nichepharma.tcitapp.View.Adapter;

import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Customer;
import com.nichepharma.tcitapp.View.Callback.IOnCustomerClicked;
import com.nichepharma.tcitapp.View.Callback.IOnHospitalClicked;
import com.nichepharma.tcitapp.databinding.ItemDoctorBinding;

import java.util.List;

public class DoctorAdapter extends RecyclerView.Adapter<DoctorAdapter.DoctorViewHolder> {

    private List<Customer> customersList;
    private ItemDoctorBinding itemCustomerBinding;
    private IOnHospitalClicked iOnCustomerClicked;

    public DoctorAdapter(List<Customer> customersList, IOnHospitalClicked iOnHospitalClicked) {
        this.customersList = customersList;
        this.iOnCustomerClicked = iOnHospitalClicked;
    }

    @NonNull
    @Override
    public DoctorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        DoctorViewHolder viewHolder;

        itemCustomerBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_doctor, parent, false);
        viewHolder = new DoctorViewHolder(itemCustomerBinding, customersList, iOnCustomerClicked);


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull DoctorViewHolder holder, int position) {

        Customer customer = customersList.get(position);
        int number = position + 1;
        itemCustomerBinding.doctorNameTextView.setText(String.valueOf(number) + ". " + customer.getName());

        if (number % 2 != 0)
            itemCustomerBinding.doctorConstraintLayout.setBackground(itemCustomerBinding.getRoot().getResources().getDrawable(R.drawable.blue_row));
        else
            itemCustomerBinding.doctorConstraintLayout.setBackground(itemCustomerBinding.getRoot().getResources().getDrawable(R.drawable.white_row));
    }

    @Override
    public int getItemCount() {
        return customersList.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class DoctorViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemDoctorBinding itemProductBinding;
        List<Customer> productsList;
        IOnHospitalClicked iOnCustomerClicked;

        DoctorViewHolder(ItemDoctorBinding itemCustomerBinding, List<Customer> productsList, IOnHospitalClicked iOnCustomerClicked) {
            super(itemCustomerBinding.getRoot());
            this.itemProductBinding = itemCustomerBinding;
            this.productsList = productsList;
            this.iOnCustomerClicked = iOnCustomerClicked;

            itemProductBinding.doctorConstraintLayout.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.doctor_constraintLayout:
                    if (iOnCustomerClicked != null){
                        if (itemProductBinding.doctorNameTextView.getTypeface() == Typeface.DEFAULT_BOLD)
                            itemProductBinding.doctorNameTextView.setTypeface(Typeface.DEFAULT);

                        else
                            itemProductBinding.doctorNameTextView.setTypeface(Typeface.DEFAULT_BOLD);

                        iOnCustomerClicked.onDoctorClick(productsList.get(getAdapterPosition()));
                    }

                    break;
            }
        }

    }
}
