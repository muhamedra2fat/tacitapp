package com.nichepharma.tcitapp.View.Callback;

public interface IOnDeleteSynchronization {

    void onDeleteSynchronization(int position);
}
