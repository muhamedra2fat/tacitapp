package com.nichepharma.tcitapp.View.Activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.SynchronizationResponse;
import com.nichepharma.tcitapp.Utils.NetworkUtils;
import com.nichepharma.tcitapp.Utils.RecyclerViewUtils;
import com.nichepharma.tcitapp.View.Adapter.SychronizationAdapter;
import com.nichepharma.tcitapp.View.Callback.IOnDeleteSynchronization;
import com.nichepharma.tcitapp.View.Callback.IOnSyncClicked;
import com.nichepharma.tcitapp.View.Callback.IShowSyncDialog;
import com.nichepharma.tcitapp.ViewModel.SychronizationViewModel;
import com.nichepharma.tcitapp.databinding.ActivitySychronizationBinding;

import java.util.List;

public class SychronizationActivity extends AppCompatActivity implements IOnSyncClicked,
        View.OnClickListener, IShowSyncDialog, IOnDeleteSynchronization {
    private SychronizationViewModel sychronizationViewModel;
    private ActivitySychronizationBinding activitySychronizationBinding;
    private SychronizationAdapter sychronizationAdapter;
    private boolean isFirstTime = true;

    private void init() {
        sychronizationViewModel = ViewModelProviders.of(this).get(SychronizationViewModel.class);
    }

    private void setListeners() {
        activitySychronizationBinding.homeImageView.setOnClickListener(this);
        activitySychronizationBinding.constraintLayout.setOnClickListener(this);
        activitySychronizationBinding.syncDialogInclude.closeSyncDialogImageView.setOnClickListener(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySychronizationBinding = DataBindingUtil.setContentView(this, R.layout.activity_sychronization);

        init();
        setListeners();

        List<Synchronization> synchronizationList = sychronizationViewModel.getSynchronizationList();
        sychronizationAdapter = new SychronizationAdapter(synchronizationList,
                this, this, this,this);

        RecyclerViewUtils.setupRecyclerView(this,
                sychronizationAdapter,
                activitySychronizationBinding.sessionsRecyclerView,
                true);
    }

    @Override
    public void syncClicked(final Synchronization synchronization, final int position) {
        if (!NetworkUtils.isNetworkAvailable(this)) {
            Toast.makeText(this, "Please check internet connection", Toast.LENGTH_SHORT).show();
            return;
        }
        sychronizationViewModel.sendVisitsAndCalls(synchronization);
        sychronizationViewModel.getSynchronizationResponseLiveData().observe(this, new Observer<SynchronizationResponse>() {
            @Override
            public void onChanged(@Nullable SynchronizationResponse synchronizationResponse) {
                if (synchronizationResponse != null) {
                    if (synchronizationResponse.getStatus().equals("success")) {
                        Toast.makeText(SychronizationActivity.this, "Successfully sent", Toast.LENGTH_SHORT).show();
                        onDeleteSynchronization(position);
                    } else
                        Toast.makeText(SychronizationActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(SychronizationActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.home_imageView:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;

            case R.id.back_imageView:
                onBackPressed();
                break;
            case R.id.constraintLayout:
                activitySychronizationBinding.syncDialogRelativeView.setVisibility(View.GONE);
                break;
            case R.id.closeSyncDialog_imageView:
                activitySychronizationBinding.syncDialogRelativeView.setVisibility(View.GONE);
                break;
        }
    }



    @Override
    public void showSyncDialog(View view, Synchronization synchronization) {
        if (!isFirstTime) {
            int[] originalPos = new int[2];
            view.getLocationInWindow(originalPos);
            activitySychronizationBinding.syncDialogRelativeView.setY(originalPos[1]);

        } else {
            isFirstTime = false;
            activitySychronizationBinding.syncDialogRelativeView.setY(view.getY());
        }
        activitySychronizationBinding.syncDialogRelativeView.setVisibility(View.VISIBLE);
        activitySychronizationBinding.syncDialogInclude.sessionDateTextView.setText(synchronization.getId());
        if (synchronization.getCustomer() != null)
            activitySychronizationBinding.syncDialogInclude.doctorsTextView.setText(synchronization.getCustomer().getName());
        else
            activitySychronizationBinding.syncDialogInclude.doctorsTextView.setText("You need to add customer");
    }

    @Override
    public void onDeleteSynchronization(int position) {
        List<Synchronization> sychronizations = sychronizationAdapter.removeItem(position);
        sychronizationViewModel.saveSynchronizationList(sychronizations);
    }
}
