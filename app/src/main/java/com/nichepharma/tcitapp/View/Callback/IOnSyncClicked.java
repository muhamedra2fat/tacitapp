package com.nichepharma.tcitapp.View.Callback;

import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;

public interface IOnSyncClicked {
    void syncClicked(Synchronization synchronization,int position);


}
