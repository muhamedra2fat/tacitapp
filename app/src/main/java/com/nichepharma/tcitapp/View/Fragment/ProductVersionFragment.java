package com.nichepharma.tcitapp.View.Fragment;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Product;
import com.nichepharma.tcitapp.Repository.Model.Versions.Version;
import com.nichepharma.tcitapp.Repository.Model.Versions.VersionResponse;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.Utils.FilesUtils;
import com.nichepharma.tcitapp.Utils.NetworkUtils;
import com.nichepharma.tcitapp.Utils.RecyclerViewUtils;
import com.nichepharma.tcitapp.View.Adapter.ProductVersionAdapter;
import com.nichepharma.tcitapp.View.Callback.IVersionHelper;
import com.nichepharma.tcitapp.ViewModel.ProductViewModel;
import com.nichepharma.tcitapp.databinding.FragmentProductVersionBinding;
import com.nichepharma.tcitapp.databinding.FragmentProductsBinding;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductVersionFragment extends Fragment implements IVersionHelper, View.OnClickListener {

    private ProductViewModel productViewModel;
    private FragmentProductVersionBinding fragmentProductsBinding;
    private ProductVersionAdapter productVersionAdapter;
    private List<Version> versionList= new ArrayList<>();

    private void init() {
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);

        productVersionAdapter = new ProductVersionAdapter(this, new ArrayList<Version>());

        RecyclerViewUtils.setupRecyclerView(getContext(),
                productVersionAdapter,
                fragmentProductsBinding.productsRecyclerView,
                true);
    }

    private void setListeners() {
        fragmentProductsBinding.backImageView.setOnClickListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentProductsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_version, container, false);
        init();
        setListeners();

        observeViewModel();
        return fragmentProductsBinding.getRoot();
    }

    private void observeViewModel() {
        productViewModel.getGetVersion();
        productViewModel.getGetVersionsLiveData().observe(this, new Observer<VersionResponse>() {
            @Override
            public void onChanged(@Nullable VersionResponse versionResponse) {
                fragmentProductsBinding.progressBar.setVisibility(View.GONE);
                if (versionResponse != null && versionResponse.getStatus().equals("success")) {
                    versionList = versionResponse.getResponse();
                    productVersionAdapter.setVersionsList(versionResponse.getResponse());
                } else
                    Toast.makeText(getContext(), "SomeThing went wrong", Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public double getCurrentVersion(String productId) {
        List<Product> productList = productViewModel.getProductsFromSharedPrefernce();
        double currentVersion = -1;
        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getId().equals(productId)) {
                currentVersion = productList.get(i).getV();
                break;
            }
        }
        return currentVersion;
    }

    @Override
    public void updateProduct(final String productId, final double version) {
        List<Product> productList = productViewModel.getProductsFromSharedPrefernce();

        for (int i = 0; i < productList.size(); i++) {
            if (productList.get(i).getId().equals(productId)) {

                final Product product = productList.get(i);
                final String zipName = product.getPath_name();
                final String path = Constants.PRODUCT_URL + zipName;
                final ProductViewModel productViewModel = new ProductViewModel(getActivity().getApplication());
                productViewModel.downloadFile(path);
                final int finalI = i;
                productViewModel.getDownloadFileLiveData().observe(getActivity(), new Observer<ResponseBody>() {
                    @Override
                    public void onChanged(@Nullable ResponseBody responseBody) {

                        if (responseBody != null) {
                            if (FilesUtils.writeResponseBodyToDisk(getContext(), responseBody, zipName)) {
                                FilesUtils.unzipFile(getContext().getExternalFilesDir(null) + File.separator
                                        + zipName.replace(".zip", "") + File.separator + zipName, zipName);
                                product.setV(version);
                                productViewModel.updateProductFromSharedPreferences(finalI, product);
                                productVersionAdapter.setLoadingIcon(productId, Product.DOWNLOADED_STATUS);
                                productVersionAdapter.setVersionsList(versionList);

                            } else {
                                Toast.makeText(getContext(), "SomeThing went wrong", Toast.LENGTH_SHORT).show();
                                productVersionAdapter.setLoadingIcon(productId, Product.ERROR_STATUS);
                            }
                        } else {
                            productVersionAdapter.setLoadingIcon(productId, Product.ERROR_STATUS);
                            Toast.makeText(getContext(), "SomeThing went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                break;
            }
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_imageView:
                getActivity().onBackPressed();
                break;
        }
    }
}
