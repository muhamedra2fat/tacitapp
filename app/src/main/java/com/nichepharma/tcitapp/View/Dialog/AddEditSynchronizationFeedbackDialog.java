package com.nichepharma.tcitapp.View.Dialog;

import android.app.Dialog;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;


import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Utils.RecyclerViewUtils;
import com.nichepharma.tcitapp.View.Adapter.AddEditSingleProductItemFeedbackAdapter;
import com.nichepharma.tcitapp.databinding.DialogAddEditSynchronizationFeedbackBinding;

public class AddEditSynchronizationFeedbackDialog extends Dialog {

    private DialogAddEditSynchronizationFeedbackBinding dialogAddEditSynchronizationFeedbackBinding;

    private Synchronization synchronization;
    private int type;
    private Context context;

    public AddEditSynchronizationFeedbackDialog(@NonNull Context context,
                                                Synchronization synchronization, int type) {
        super(context);
        this.context = context;
        this.synchronization = synchronization;
        this.type = type;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogAddEditSynchronizationFeedbackBinding =
                DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_add_edit_synchronization_feedback, null, false);
        setContentView(dialogAddEditSynchronizationFeedbackBinding.getRoot());


        RecyclerViewUtils.setupRecyclerView(
                getContext(),
                new AddEditSingleProductItemFeedbackAdapter(synchronization, type, context),
                dialogAddEditSynchronizationFeedbackBinding.recyclerView,
                true);

    }


}
