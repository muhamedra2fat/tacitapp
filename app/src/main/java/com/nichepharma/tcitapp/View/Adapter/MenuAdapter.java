package com.nichepharma.tcitapp.View.Adapter;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Menu.MenuItem;
import com.nichepharma.tcitapp.Utils.FragmentUtils;
import com.nichepharma.tcitapp.View.Callback.IOnMenuItemClicked;
import com.nichepharma.tcitapp.View.Fragment.PDFFragment;
import com.nichepharma.tcitapp.databinding.ItemMenuBinding;
import com.nichepharma.tcitapp.databinding.ItemReferenceBinding;

import java.io.File;
import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {
    private List<MenuItem> menuItemList;
    private ItemMenuBinding itemMenuBinding;
    private IOnMenuItemClicked iOnMenuItemClicked;
    private String productName;

    public MenuAdapter(List<MenuItem> menuItemList, IOnMenuItemClicked iOnMenuItemClicked, String productName) {
        this.menuItemList = menuItemList;
        this.iOnMenuItemClicked = iOnMenuItemClicked;
        this.productName = productName;
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        MenuViewHolder viewHolder;

        itemMenuBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_menu, parent, false);
        viewHolder = new MenuViewHolder(itemMenuBinding);


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {

        if (menuItemList.get(position) != null) {
            File imgFile = new File(itemMenuBinding.getRoot().getContext().getExternalFilesDir(null) +
                    File.separator + productName + File.separator + menuItemList.get(position).getImage());
            if (imgFile.exists())
                holder.itemMenuBinding.menuImageImageView.setImageURI(Uri.fromFile(imgFile));


        }
    }


    @Override
    public int getItemCount() {
        return menuItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    class MenuViewHolder extends RecyclerView.ViewHolder {
        private ItemMenuBinding itemMenuBinding;

        MenuViewHolder(ItemMenuBinding itemMenuBinding) {
            super(itemMenuBinding.getRoot());
            this.itemMenuBinding = itemMenuBinding;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iOnMenuItemClicked.menuItemClicked(menuItemList.get(getAdapterPosition()));
                }
            });
        }


    }
}
