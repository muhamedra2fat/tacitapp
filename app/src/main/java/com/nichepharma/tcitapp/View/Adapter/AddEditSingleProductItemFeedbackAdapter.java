package com.nichepharma.tcitapp.View.Adapter;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Synchronization.Synchronization;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.View.Activity.FeedbackActivity;
import com.nichepharma.tcitapp.databinding.ItemAddEditFeedbackBinding;

public class AddEditSingleProductItemFeedbackAdapter extends RecyclerView.Adapter<AddEditSingleProductItemFeedbackAdapter.AddEditSingleProductFeedbackViewHolder> {

    private Synchronization synchronization;
    private ItemAddEditFeedbackBinding itemAddEditFeedbackBinding;
    private int type;
    private Context context;

    public AddEditSingleProductItemFeedbackAdapter(Synchronization synchronization, int type, Context context) {
        this.synchronization = synchronization;
        this.type = type;
        this.context = context;
    }

    @NonNull
    @Override
    public AddEditSingleProductFeedbackViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        AddEditSingleProductFeedbackViewHolder viewHolder;

        itemAddEditFeedbackBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_add_edit_feedback, parent, false);
        viewHolder = new AddEditSingleProductFeedbackViewHolder();


        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull AddEditSingleProductFeedbackViewHolder holder, int position) {
        if (type == Constants.ADD_FEEDBACK) {

            if (synchronization.getCalls().get(position).getCallRemark() == null ||
                    synchronization.getCalls().get(position).getSamples() == 0)
                itemAddEditFeedbackBinding.productNameTextView.setText(synchronization.getCalls().get(position).getProduct().getName());
            else
                itemAddEditFeedbackBinding.productNameTextView.setVisibility(View.GONE);
        } else {
            if (synchronization.getCalls().get(position).getCallRemark() == null ||
                    synchronization.getCalls().get(position).getSamples() == 0)
                itemAddEditFeedbackBinding.productNameTextView.setVisibility(View.GONE);
            else
                itemAddEditFeedbackBinding.productNameTextView.setText(synchronization.getCalls().get(position).getProduct().getName());


        }
    }

    @Override
    public int getItemCount() {
        return synchronization.getCalls().size();
    }

    class AddEditSingleProductFeedbackViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AddEditSingleProductFeedbackViewHolder() {
            super(itemAddEditFeedbackBinding.getRoot());
            itemAddEditFeedbackBinding.productNameTextView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.productName_textView:
                    Intent intent = new Intent(itemAddEditFeedbackBinding.getRoot().getContext(), FeedbackActivity.class);
                    intent.putExtra(Constants.SYNCHRONIZATION_OBJ, synchronization);
                    intent.putExtra(Constants.SYNC_POSITION, getAdapterPosition());
                    itemAddEditFeedbackBinding.getRoot().getContext().startActivity(intent);
                    ((AppCompatActivity) context).finish();
                    break;
            }
        }

    }
}
