package com.nichepharma.tcitapp.View.Adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nichepharma.tcitapp.R;
import com.nichepharma.tcitapp.Repository.Model.Customer;
import com.nichepharma.tcitapp.View.Callback.IOnCustomerClicked;
import com.nichepharma.tcitapp.databinding.ItemCustomerBinding;

import java.util.List;

public class UserCustomersAdapter extends RecyclerView.Adapter<UserCustomersAdapter.UserCustomersViewHolder> {

    private List<Customer> customersList;
    private ItemCustomerBinding itemCustomerBinding;
    private IOnCustomerClicked iOnCustomerClicked;


    public UserCustomersAdapter(List<Customer> customersList, IOnCustomerClicked iOnCustomerClicked) {
        this.customersList = customersList;
        this.iOnCustomerClicked = iOnCustomerClicked;
    }

    @NonNull
    @Override
    public UserCustomersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        UserCustomersViewHolder viewHolder;

        itemCustomerBinding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_customer, parent, false);
        viewHolder = new UserCustomersViewHolder(itemCustomerBinding, customersList, iOnCustomerClicked);


        return viewHolder;

    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull UserCustomersViewHolder holder, int position) {

        Customer customer = customersList.get(position);
        itemCustomerBinding.customerNameTextView.setText(customer.getName());
        itemCustomerBinding.customerSpecialityTextView.setText(customer.getSpeciality());
        itemCustomerBinding.customerConstraintLayout.setBackground(holder.itemProductBinding.getRoot().getResources().getDrawable(R.drawable.single_customer_row));
    }

    @Override
    public int getItemCount() {
        return customersList.size();
    }


    static class UserCustomersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ItemCustomerBinding itemProductBinding;
        List<Customer> customerList;
        IOnCustomerClicked iOnCustomerClicked;
        int clickedPosition = -1;


        private static ConstraintLayout constraintLayout;

        UserCustomersViewHolder(ItemCustomerBinding itemCustomerBinding, List<Customer> customerList, IOnCustomerClicked iOnCustomerClicked) {
            super(itemCustomerBinding.getRoot());
            this.itemProductBinding = itemCustomerBinding;
            this.customerList = customerList;
            this.iOnCustomerClicked = iOnCustomerClicked;
            itemCustomerBinding.customerConstraintLayout.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.customer_constraintLayout:
                    int adapterPos = getAdapterPosition();
                    if (adapterPos != -1) {
                        if (constraintLayout != null)
                            constraintLayout.setBackground(itemProductBinding.getRoot().getResources().getDrawable(R.drawable.single_customer_row));

                        itemProductBinding.customerConstraintLayout.setBackground(itemProductBinding.getRoot().getResources().getDrawable(R.drawable.selected_doctor));

                        constraintLayout = itemProductBinding.customerConstraintLayout;

                        iOnCustomerClicked.onCustomerClick(customerList.get(adapterPos));
                    }
                    break;
            }
        }

    }
}
