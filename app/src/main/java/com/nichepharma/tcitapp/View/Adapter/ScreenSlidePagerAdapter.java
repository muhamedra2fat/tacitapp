package com.nichepharma.tcitapp.View.Adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.nichepharma.tcitapp.Repository.Model.SlideshowsItem;
import com.nichepharma.tcitapp.Utils.Constants;
import com.nichepharma.tcitapp.View.Fragment.SlideFragment;

import java.util.List;

/**
 * A simple pager adapter that represents  ScreenSlidePageFragment objects, in
 * sequence.
 */
public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    private List<SlideshowsItem> slideshowsItems;
    private String productName;

    public ScreenSlidePagerAdapter(FragmentManager fm, List<SlideshowsItem> slideshowsItems, String productName) {
        super(fm);
        this.slideshowsItems = slideshowsItems;
        this.productName = productName;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("slide", slideshowsItems.get(position));
        bundle.putString(Constants.PRODUCT_NAME, productName);
        SlideFragment slideFragment = new SlideFragment();
        slideFragment.setArguments(bundle);
        return slideFragment;

    }

    @Override
    public int getCount() {
        return slideshowsItems.size();
    }
}
